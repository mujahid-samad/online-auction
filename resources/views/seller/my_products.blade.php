@extends('layouts.sellers_base_layout')
@section('content')
    <div class="container-fluid">
        <div class="">
            {{-- <div class="main-panel">--}}
            @include('layouts.partials.sellers_nav')
            @include('layouts.partials.header')
            <div class="row">

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))

                                    <p class="alert alert-{{ $msg }}">
                                        @if($msg=='success')
                                            <img src="{{URL::asset('/public/icons/tick.png')}}" height="35px">
                                        @else
                                            <img src="{{URL::asset('/public/icons/cross.png')}}" height="35px">
                                        @endif

                                        <?php echo Session::get('alert-' . $msg); ?> <a href="#"
                                                                                        class="close"
                                                                                        data-dismiss="alert"
                                                                                        aria-label="close">&times;</a>
                                    </p>
                                @endif
                            @endforeach
                        </div> <!-- end .flash-message -->
                        <div class="panel panel-default panel-table">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col col-xs-2">
                                        <h3 class="panel-title">My Products</h3>
                                    </div>
                                    <div class="col col-xs-8">
                                        Category :
                                        <select id="mySelect" onchange="myFunction()">
                                            <?php
                                            if(isset($_GET['category'])) {
                                                $category =  $_GET['category'];
                                            }else{
                                                $category = "all";
                                            }

                                            $categories = \App\Category::select('name')->get()->all();
                                            $categories = array_column($categories,'name');
                                            array_push( $categories,'all');
                                            ?>

                                            @foreach ($categories as $c)

                                                @if($c == $category)

                                                    <option value="<?php echo $c;?>"
                                                            selected="selected"><?php echo $c;?></option>
                                                @else
                                                    <option value="<?php echo $c;?>"><?php echo $c;?></option>
                                                @endif
                                            @endforeach

                                        </select>

                                        <?php
                                        if( $category=='all') {
                                            $products = Auth::user()->Product()->orderBy('created_at', 'desc')->paginate(10);
                                        }else{
                                            $products = Auth::user()->Product()->where('category',$category)->orderBy('created_at', 'desc')->paginate(10);
                                        }
                                        $i=0;
                                        ?>

                                        <script>
                                            function myFunction() {
                                                var x = document.getElementById("mySelect").value;
                                                window.location.href = '?category=' + x;
                                            }

                                        </script>

                                    </div>

                                    <div class="col col-xs-2 text-right">
                                        <a href="{{ url('/create_product')}}">
                                            <button type="button" class="btn btn-sm btn-primary btn-create">Create
                                                New
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered table-list">
                                    <thead>
                                    <tr>
                                        <th class="text-center">SL</th>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <th>Product Name</th>
                                        <th>Minimum Bid</th>
                                        <th>Bid Endtime</th>
                                        <th><em class="fa fa-address-card-o"></em></th>
                                        <th><em class="fa fa-cog"></em></th>
                                        <th><em class="fa fa-remove"></em></th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = $products->perPage() * ($products->currentPage() - 1);?>
                                    @foreach($products as $product)
                                        <tr>
                                            <td class="text-center">{{++$i}}</td>
                                            <td><a href="{{url('seller/product_desc/'.$product->id)}}"><img
                                                            width="70px"
                                                            height="70px"
                                                            src="{{URL::asset('public/'.$product->image1)}}"
                                                    ></a>
                                            </td>
                                            <td>{{$product->category}}</td>
                                            <td>{{$product->name}}</td>
                                            <td>{{$product->price}}</td>
                                            <td>{{$product->end_time}}</td>
                                            <td><a href="{{url('seller/product_desc/'.$product->id)}}"
                                                   class="btn btn-sm">
                                                    View
                                                </a>
                                            </td>

                                            <td>
                                                <a href="{{url('/edit_product/'.$product->id)}}"
                                                   class="btn btn-sm"><em class="fa fa-pencil"></em></a>
                                            </td>

                                            <td>
                                                <a onclick="return confirm('Are you sure?')"
                                                   href="{{url('/delete_product/'.$product->id)}}"
                                                   class="btn btn-sm"><em class="fa fa-trash"></em></a>
                                            </td>

                                            <td>
                                                <?php
                                                $end_time = new DateTime($product->end_time);
                                                $now = new DateTime();

                                                $bids = $product->bid;
                                                $awarded = false;
                                                foreach ($bids as $bid) {
                                                    $awarded = $bid->awarded;
                                                }
                                                ?>
                                                <span>
                                                        <?php
                                                    if ($awarded)
                                                        echo "Awarded";
                                                    else if ($now < $end_time)
                                                        echo 'Active';
                                                    else echo 'Bid Ended';
                                                    ?>
                                                    </span>
                                            </td>

                                        </tr>
                                    @endforeach
                                    {{-- {{ $products->links() }}--}}
                                    </tbody>
                                </table>

                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col col-xs-8">
                                        {{ $products->Links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('layouts.partials.header')
            @include('layouts.partials.footer')
        </div>
    </div>
    </div>
@endsection