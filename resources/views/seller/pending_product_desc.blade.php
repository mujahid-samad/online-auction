@extends('layouts.sellers_base_layout')
@section('content')
    <div class="container">
        @include('layouts.partials.sellers_nav')
        @include('layouts.partials.header')
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))

                        <p class="alert alert-{{ $msg }}">
                            @if($msg=='success')
                                <img src="{{URL::asset('/public/icons/tick.png')}}" height="35px">
                            @else
                                <img src="{{URL::asset('/public/icons/cross.png')}}" height="35px">
                            @endif

                            <?php echo Session::get('alert-' . $msg); ?> <a href="#"
                                                                            class="close"
                                                                            data-dismiss="alert"
                                                                            aria-label="close">&times;</a>
                        </p>
                    @endif
                @endforeach
            </div>
            <!-- end .flash-message -->
            <div class="well">
                <div class="card">
                    <h3><p class="text-center">Product Bid</p></h3>
                    <img src="{{URL::asset('public/'.$product->image1)}}" class="center-block" alt="..."
                         style="width: 300px; height: 300px;">
                    <br>
                    <table class="table borderless" style="width:100%">
                        <style>
                            textarea {
                                max-width: 100%;
                                max-height: 100%;
                                resize: none;
                            }
                        </style>
                        <tr>
                            <td>Product Name</td>
                            <td>:</td>
                            <td>{{$product->name}}</td>
                        </tr>
                        <tr>
                            <td>Product Description</td>
                            <td>:</td>
                            <td><textarea rows="4" cols="50" class="field left"
                                          readonly>{{$product->description}}</textarea></td>
                        </tr>
                        <tr>
                            <td>Category</td>
                            <td>:</td>
                            <td>{{$product->category}}</td>
                        </tr>
                        <tr>
                            <td>Minimum Bid</td>
                            <td>:</td>
                            <td>{{$product->price}}</td>
                        </tr>
                        <tr>
                            <td>Bid Endtime</td>
                            <td>:</td>
                            <td>{{$product->end_time}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-2">

            </div>
        </div>
        @include('layouts.partials.header')
        @include('layouts.partials.footer')
    </div>
@endsection

