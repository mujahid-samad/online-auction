@extends('layouts.sellers_base_layout')
@section('content')

    <div class="container-fluid">
        <div class="">
            <div class="">
                @include('layouts.partials.sellers_nav')
                @include('layouts.partials.header')
                <div class="row">

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="flash-message">
                                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                    @if(Session::has('alert-' . $msg))

                                        <p class="alert alert-{{ $msg }}">
                                            @if($msg=='success')
                                                <img src="{{URL::asset('/public/icons/tick.png')}}" height="35px">
                                            @else
                                                <img src="{{URL::asset('/public/icons/cross.png')}}" height="35px">
                                            @endif

                                            <?php echo Session::get('alert-' . $msg); ?> <a href="#"
                                                                                            class="close"
                                                                                            data-dismiss="alert"
                                                                                            aria-label="close">&times;</a>
                                        </p>
                                    @endif
                                @endforeach
                            </div> <!-- end .flash-message -->
                            <div class="panel panel-default panel-table">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col col-xs-6">

                                        </div>
                                        <div class="col col-xs-6 text-right">

                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-striped table-bordered table-list">
                                        <thead>
                                        <tr>
                                            <th class="text-center">SL</th>
                                            <th>Bidder's Name</th>
                                            <th>Bid Price</th>
                                            <th>Award</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = $bids->perPage() * ($bids->currentPage() - 1);?>
                                        @foreach($bids as $bid)
                                            <tr>
                                                <td class="text-center">{{++$i}}</td>
                                                <td>{{$bid->user->name}}</td>
                                                <td>{{$bid->price}}</td>
                                                <td>
                                                    @if(!$bid->awarded)
                                                        <a class="btn btn-success delete-court-button"
                                                           role="button"
                                                           data-action-link="{{url('public/award/'.$bid->id)}}"
                                                           data-toggle="modal"
                                                           data-target="#modal-delete">Award</a>
                                                    @else
                                                        <a class="btn btn-white delete-court-button">Awarded</a>
                                                    @endif
                                                </td>

                                            </tr>
                                            @php $i++; @endphp
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>
                                <div class="panel-footer">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <form id="delete-court-form" method="POST" action="">

                                {{csrf_field()}}

                                <div class="modal-header text-center">
                                    <h4 class="modal-title w-100 font-weight-bold">Are You Sure?</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-footer d-flex justify-content-center">
                                    <input type="submit" value="Award">
                                </div>

                            </form>

                        </div>

                    </div>

                    <script>
                        $('.delete-court-button').on('click', function () {
                            $('#delete-court-form').attr('action', $(this).data('action-link'));
                        });
                    </script>

                </div>

                @include('layouts.partials.footer')
            </div>
        </div>
    </div>
@endsection
