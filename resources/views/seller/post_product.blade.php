@extends('layouts.sellers_base_layout')
@section('content')
    <div class="container-fluid">
        <div class="wrapper">
            <div class="container">
                @include('layouts.partials.sellers_nav')
                @include('layouts.partials.header')
                <div class="row">



                    <div class="col-md-2">

                    </div>
                        <div class="col-md-8">
                            <div class="flash-message">
                                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                    @if(Session::has('alert-' . $msg))

                                        <p class="alert alert-{{ $msg }}">
                                            @if($msg=='success')
                                                <img src="{{URL::asset('/public/icons/tick.png')}}" height="35px">
                                            @else
                                                <img src="{{URL::asset('/public/icons/cross.png')}}" height="35px">
                                            @endif

                                            <?php echo Session::get('alert-' . $msg); ?> <a href="#"
                                                                                            class="close"
                                                                                            data-dismiss="alert"
                                                                                            aria-label="close">&times;</a>
                                        </p>
                                    @endif
                                @endforeach
                            </div>
                            <!-- end .flash-message -->
                            <div class="card card-register mx-auto mt-5">
                                <div align="center" class="card-header">Post a product</div>
                                <div class="card-body">

                                    <form class="form-horizontal" method="POST" action="{{ route('post_product') }}"
                                          enctype="multipart/form-data">

                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="name">Product's Name</label>
                                            <input class="form-control" id="name" name="name" value="{{ old('name') }}"
                                                   type="text"
                                                   aria-describedby="nameHelp" required>
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <div class="form-row">

                                                <div class="col-md-4">
                                                    <label for="category">Category</label>
                                                    <select class="form-control" id="category" name="category"
                                                            type="text"
                                                            aria-describedby="userType">

                                                        @foreach($categories as $category)
                                                            <option value="{{$category->name}}">{{$category->name}}</option>
                                                        @endforeach

                                                    </select>

                                                </div>

                                                <div class="col-md-4">
                                                    <label for="price">Price(BDT)</label>
                                                    <input class="form-control" id="price" name="price" price="price"
                                                           value="{{ old('price') }}"
                                                           type="text" aria-describedby="priceHelp" required>
                                                    @if ($errors->has('price'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('price') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                                <div class="col-md-4">
                                                    <label for="price">Ending Date</label>
                                                    <input class="form-control" id="price" name="end_time"
                                                           price="end_time"
                                                           value="{{ old('end_time') }}"
                                                           type="date" aria-describedby="priceHelp">
                                                    @if ($errors->has('end_time'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('end_time') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="description">Product's Description</label>
                                            <textarea class="form-control rounded-0"
                                                      id="description" name="description"
                                                      value="{{ old('description') }}"
                                                      type="text" rows="3" required>
                                            </textarea>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('description') }}</strong>
                                                    </span>
                                            @endif
                                        </div>


                                        <div class="container-fluid">
                                            <label for="image1">Select Picture</label>

                                            <input type="file" name="image1" accept="image/*" required>
                                        </div>
                                        <button class="btn btn-default btn-block">Submit</button>
                                    </form>
                                </div>
                            </div>
                            <div class="text-center">
                                <br><br>
                                <br><br>
                            </div>
                        </div>
                    <div class="col-md-2">

                    </div>
                </div>
                @include('layouts.partials.footer')
            </div>
        </div>
    </div>

@endsection
