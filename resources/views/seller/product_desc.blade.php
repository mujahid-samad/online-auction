@extends('layouts.sellers_base_layout')
@section('content')
    <style>
        .borderless td, .borderless th {
            border: none;
        }

        ul {
            list-style: none;
        }
    </style>

    <div class="container">
        @include('layouts.partials.sellers_nav')
        @include('layouts.partials.header')

        <div class="col-md-2">

        </div>

        <div class="col-md-8">
            <div class="well">
                <div class="card">
                    @php
                        $bids = \App\Product::find($product->id)->Bid()->get()->all();
                        $max = 0;
                        foreach ($bids as $bid){
                         if($bid->price>$max){
                           $max = $bid->price;
                         }
                        }
                    @endphp

                    <h3><p class="text-center">Product Bid</p></h3>
                    <img src="{{URL::asset('public/'.$product->image1)}}" class="center-block" alt="..."
                         style="width: 300px; height: 300px;">
                    <br>
                    <table class="table borderless" style="width:100%">
                        <style>
                            textarea {
                                max-width: 100%;
                                max-height: 100%;
                                resize: none;
                            }
                        </style>
                        <tr>
                            <td>Product Name</td>
                            <td>:</td>
                            <td>{{$product->name}}</td>
                        </tr>
                        <tr>
                            <td>Product Description</td>
                            <td>:</td>
                            <td><textarea rows="4" cols="50" class="field left"
                                          readonly>{{$product->description}}</textarea></td>
                        </tr>
                        <tr>
                            <td>Category</td>
                            <td>:</td>
                            <td>{{$product->category}}</td>
                        </tr>
                        <tr>
                            <td>Minimum Bid</td>
                            <td>:</td>
                            <td>{{$product->price}}</td>
                        </tr>
                        <tr>
                            <td>Bid Endtime</td>
                            <td>:</td>
                            <td>{{$product->end_time}}</td>
                        </tr>
                        <tr>
                            <td>Total Bid Count</td>
                            <td>:</td>
                            <td><a href="{{url('/see_bidders/'.$product->id)}}">{{count($bids)}}</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Max Bid</td>
                            <td>:</td>
                            <td>{{$max}}</td>
                        </tr>
                    </table>
                    <div class="container-fluid">
                        <div class="row top-buffer">
                            <a role="button" href="{{url('/see_bidders/'.$product->id)}}">
                                <button type="button" class="btn center-block btn-success large">
                                    See Bidders
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2">

            </div>
        </div>
        @include('layouts.partials.header')
        @include('layouts.partials.footer')
    </div>
@endsection

