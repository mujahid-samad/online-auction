<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.partials.head')
</head>

<body class="bg-dark">
<div class="container">
    <div class="card card-register mx-auto col-md-8">
        <div class="card-header">Register an Account</div>
        <div class="card-body">

            <form class="form-horizontal" method="POST" action="{{ route('register') }}" enctype="multipart/form-data" >
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <div class="form-row">

                        <div class="col-md-6">
                            <label for="name">Name</label>
                            <input class="form-control" id="name" name="name" value="{{ old('name') }}" type="text"
                                   aria-describedby="nameHelp"
                                   placeholder="Enter name" required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="col-md-6">
                            <label for="user_type">User Type</label>
                            <select class="form-control" id="user_type" name="user_type" type="text"
                                    aria-describedby="userType" placeholder="User Type?">
                                <option value="buyer">Buyer</option>
                                <option value="seller">Seller</option>
                            </select>

                        </div>

                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">Email address</label>
                    <input class="form-control" id="email" name="email" value="{{ old('email') }}" type="email"
                           aria-describedby="emailHelp"
                           placeholder="Enter email" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label for="phone">Phone</label>

                    <input id="phone" name="phone" type="text" placeholder="Enter Phone number" class="form-control"
                           value="{{ old('phone') }}" required>

                    @if ($errors->has('phone'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                    @endif

                </div>

                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <label for="address">Address</label>


                    <input id="address" name="address" type="text" placeholder="Enter Address" class="form-control"
                           value="{{ old('address') }}" required>

                    @if ($errors->has('address'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                    @endif

                </div>


                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="password">Password</label>
                            <input class="form-control" name="password" id="password" value="{{ old('password') }}"
                                   type="password"
                                   placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="confirm_pasword">Confirm password</label>
                            <input class="form-control" name="password_confirmation" id="password_confirmation"
                                   type="password"
                                   placeholder="Confirm password">

                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <label for="image">Profile Picture</label>
                    <input type="file" name="image" accept="image/*" alt="alternative text" title="Profile Picture"
                           required>
                </div>

                <button class="btn btn-primary btn-block">Register</button>
            </form>
            <div class="text-right">
                <br>
                <a class="btn btn-info" href="{{route('login')}}"> Close</a>
            </div>
        </div>
    </div>
</div>


<script src=" {!!asset('admin/vendor/jquery/jquery.min.js')!!} " }></script>
<script src=" {!!asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')!!} " }></script>
<script src=" {!!asset('admin/vendor/jquery-easing/jquery.easing.min.js')!!} " }></script>

</body>

</html>