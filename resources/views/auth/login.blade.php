@extends('layouts.baselayout')

@section('content')
    <style>
        img:hover {
            animation: shake 0.5s;
            animation-iteration-count: infinite;
        }

        @keyframes shake {
            0% { transform: translate(1px, 1px) rotate(0deg); }
            10% { transform: translate(-1px, -2px) rotate(-1deg); }
            20% { transform: translate(-3px, 0px) rotate(1deg); }
            30% { transform: translate(3px, 2px) rotate(0deg); }
            40% { transform: translate(1px, -1px) rotate(1deg); }
            50% { transform: translate(-1px, 2px) rotate(-1deg); }
            60% { transform: translate(-3px, 1px) rotate(0deg); }
            70% { transform: translate(3px, 1px) rotate(-1deg); }
            80% { transform: translate(-1px, -1px) rotate(1deg); }
            90% { transform: translate(1px, 2px) rotate(0deg); }
            100% { transform: translate(1px, -2px) rotate(-1deg); }
        }
    </style>
    <!-- Page Content -->
    <div class="container">
        <!-- Heading Row -->
        <div class="row my-4">
            <div class="col-lg-8">
                <img class="img-fluid rounded" src="{{asset('public/admin/img/Online_Auction_Logo.png')}}" height="900%"
                     width="400%" alt="">
            </div>
            <!-- /.col-lg-8 -->
            <div class="col-lg-4">
                <h1>Online Auction System</h1>
                <p>An online auction is an auction which is held over the internet. Online auctions come in many
                    different formats, but most popularly they are ascending English auctions, descending Dutch
                    auctions, first-price sealed-bid, Vickrey auctions, or sometimes even a combination of multiple
                    auctions, taking elements of one and forging them with another.</p>
                <a class="btn btn-primary btn-lg" href="{{'about'}}">See More</a>
            </div>
            <!-- /.col-md-4 -->
        </div>
        <!-- /.row -->

        <!-- Call to Action Well -->
        <div class="card text-white bg-secondary my-4 text-center">
            <div class="card-body">
                <p class="text-white m-0"><marquee><h5><b>Some of the most common types of auctions are live auction, silent auction and
                                online auction.</b></h5></marquee></p>
            </div>
        </div>
        <!-- Content Row -->
        <div class="row">
            <?php $products = \App\Product::orderBy('created_at','desc')->limit(9)->get();
            ?>
            @foreach($products as $product)
                <div class="col-md-4 mb-4">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title"><img width="150px" height="130px"   class="rounded mx-auto d-block" src="{{URL::asset('public/'.$product->image1)}}"
                                                         > </h2>
                            <p class="card-text">Product Name &nbsp;   : &nbsp;&nbsp; {{$product->name}} <br>
                                Product Price     &nbsp; &nbsp;&nbsp;: &nbsp;&nbsp;  {{$product->price}}<br>
                                
                           {{$product->description}}</p>
                        </div>
                        <div class="card-footer">
                            <a class="btn btn-primary" href="#loginModal" data-toggle="modal" data-target="#loginModal">Bid Now</a>
                        </div>
                    </div>
                </div>
        @endforeach
        <!-- /.col-md-4 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
@endsection