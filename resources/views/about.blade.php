@extends('layouts.baselayout')
        @section('content')
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <br>
                        <br>
                        <br>
                        <br>
                        <img class="img-fluid rounded" src="{{asset('public/admin/ad/Home_delivery_scooter.jpg')}}"
                             {{--height="900%" width="400%"--}} alt="">
                        <br>
                        <br>
                        <br>
                        <br>
                        <img class="img-fluid rounded" src="{{asset('public/admin/ad/ad-here.gif')}}"
                             {{--height="900%" width="400%"--}} alt="">
                        <br>
                        <br>
                        <br>
                        <br>
                        <img class="img-fluid rounded" src="{{asset('public/admin/ad/signup.gif')}}"
                             {{--height="900%" width="400%"--}} alt="">

                    </div>
                    <div class="col-md-8">
                        <br>
                        <h3>History</h3>
                        <p>
                            Online auctions were taking place even before the release of the first web browser for personal
                            computers, NCSA Mosaic. Instead of users selling items through the Web they were instead trading through
                            text-based newsgroups and email discussion lists. However, the first Web-based commercial activity
                            regarding online auctions that made significant sales began in May 1995 with the company Onsale. In
                            September that same year eBay also began trading. Both of these companies used ascending bid. The Web
                            offered new advantages such as the use of automated bids via electronic forms, a search engine to be
                            able to quickly find items and the ability to allow users to view items by categories.

                            Online auctions have greatly increased the variety of goods and services that can be bought and sold
                            using auction mechanisms along with expanding the possibilities for the ways auctions can be conducted
                            and in general created new uses for auctions. In the current web environment there are hundreds, if not
                            thousands, of websites dedicated to online auction practices.
                        </p>
                        <h3>Legalities</h3>
                        <h5>Shill bidding</h5>
                        <p>Placing fake bids that benefits the seller of the item is known as shill bidding. This is a method often
                            used in Online auctions but can also happen in standard auctions. This is seen as an unlawful act as it
                            unfairly raises the final price of the auction, so that the winning bidder pays more than they should
                            have. If the shill bid is unsuccessful, the item owner needs to pay the auction fees. In 2011, a member
                            of eBay became the first individual to be convicted of shill bidding on an auction.[15] By taking part
                            in the process, an individual is breaking the European Union fair trading rules which carries out a fine
                            of up to £5,000 in the United Kingdom.
                        </p>
                        <h5>Fraud</h5>
                        <p>
                            The increasing popularity of using online auctions has led to an increase in fraudulent activity.[2]
                            This is usually performed on an auction website by creating a very appetising auction, such as a low
                            starting amount. Once a buyer wins an auction and pays for it, the fradulent seller will either not
                            pursue with the delivery,[17] or send a less valuable version of the purchased item (replicated, used,
                            refurbished, etc.). Protection to prevent such acts has become readily available, most notably Paypal's
                            buyer protection policy. As Paypal handles the transaction, they have the ability to hold funds until a
                            conclusion is drawn whereby the victim can be compensated.
                        </p>
                        <h5>Sale of stolen goods</h5>
                        <p>
                            Online auction websites are used by thieves or fences to sell stolen goods to unsuspecting buyers.[19]
                            According to police statistics there were over 8000 crimes involving stolen goods, fraud or deception
                            reported on eBay in 2009.[20] It has become common practice for organised criminals to steal in-demand
                            items, often in bulk. These items are then sold online as it is a safer option due to the anonymity and
                            worldwide market it provides.[21] Auction fraud makes up a large percentage of complaints received by
                            the FBI’s Internet Crime Complaint Center (IC3). This was around 45% in 2006 and 63% in 2005
                        </p>
                        <h3>Bidding techniques</h3>
                        <h5>Auction sniping</h5>
                        <p>
                            Auction sniping is a controversial bidding technique used in timed online auctions.[23] It is the
                            practice of placing a bid in the final stages of an auction with the aim of removing other bidder's
                            ability to place another bid before the auction ends. These bids can either be placed by the bidder
                            manually or automatically with the use of a tool.[24] There are tools available that have been developed
                            for this purpose. However, the use of these tools is the subject of much controversy.[25]

                            There are two different approaches employed by sniping tools.
                        <ul>
                            <li>
                                Online: These are hosted on a remote server and are a service run by a third party.
                            </li>
                            <li>
                                Local: This type is a script which can be downloaded onto the users computer which is then activated
                                and run locally.
                            </li>

                        </ul>


                        </p>
                    </div>
                    <div class="col-md-2">
                        <br>
                        <br>
                        <br>
                        <br>
                        <img class="img-fluid rounded" src="{{asset('public/admin/ad/home-delivery-job-interview.jpg')}}"
                             {{--height="900%" width="400%"--}} alt="">
                        <br>
                        <br>
                        <br>
                        <br>
                        <img class="img-fluid rounded" src="{{asset('public/admin/ad/ok1.jpg')}}"
                             {{--height="900%" width="400%"--}} alt="">
                        <br>
                        <br>
                        <br>
                        <br>
                        <img class="img-fluid rounded" src="{{asset('public/admin/ad/home-1.png')}}"
                             {{--height="900%" width="400%"--}} alt="">

                    </div>

                </div>
            </div>

        @endsection


{{--
 <html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Online Auction System</title>

    <!-- Bootstrap core CSS -->
    <link href="{!!asset('public/theme/vendor/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">

    <!--to use jquery or ajax -->
    <script src=" {!!asset('public/theme/vendor/jquery/jquery.min.js')!!} " }></script>
    <script src="{!!asset('public/theme/vendor/bootstrap/js/bootstrap.bundle.js')!!}" }></script>
    <script src="{!!asset('public/theme/vendor/bootstrap/js/bootstrap.js')!!}" }></script>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet"/>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet'
          type='text/css'>


</head>


<body>
 <div class="container">
    <div class="row">
        <div class="col-md-2">
            <br>
            <br>
            <br>
            <br>
            <img class="img-fluid rounded" src="{{asset('public/admin/ad/Home_delivery_scooter.jpg')}}"
                 --}}
{{--height="900%" width="400%"--}}{{--
 alt="">
            <br>
            <br>
            <br>
            <br>
            <img class="img-fluid rounded" src="{{asset('public/admin/ad/ad-here.gif')}}"
                 --}}
{{--height="900%" width="400%"--}}{{--
 alt="">
            <br>
            <br>
            <br>
            <br>
            <img class="img-fluid rounded" src="{{asset('public/admin/ad/signup.gif')}}"
                 --}}
{{--height="900%" width="400%"--}}{{--
 alt="">

        </div>
        <div class="col-md-8">
            <br>
            <h3>History</h3>
            <p>
                Online auctions were taking place even before the release of the first web browser for personal
                computers, NCSA Mosaic. Instead of users selling items through the Web they were instead trading through
                text-based newsgroups and email discussion lists. However, the first Web-based commercial activity
                regarding online auctions that made significant sales began in May 1995 with the company Onsale. In
                September that same year eBay also began trading. Both of these companies used ascending bid. The Web
                offered new advantages such as the use of automated bids via electronic forms, a search engine to be
                able to quickly find items and the ability to allow users to view items by categories.

                Online auctions have greatly increased the variety of goods and services that can be bought and sold
                using auction mechanisms along with expanding the possibilities for the ways auctions can be conducted
                and in general created new uses for auctions. In the current web environment there are hundreds, if not
                thousands, of websites dedicated to online auction practices.
            </p>
            <h3>Legalities</h3>
            <h5>Shill bidding</h5>
            <p>Placing fake bids that benefits the seller of the item is known as shill bidding. This is a method often
                used in Online auctions but can also happen in standard auctions. This is seen as an unlawful act as it
                unfairly raises the final price of the auction, so that the winning bidder pays more than they should
                have. If the shill bid is unsuccessful, the item owner needs to pay the auction fees. In 2011, a member
                of eBay became the first individual to be convicted of shill bidding on an auction.[15] By taking part
                in the process, an individual is breaking the European Union fair trading rules which carries out a fine
                of up to £5,000 in the United Kingdom.
            </p>
            <h5>Fraud</h5>
            <p>
                The increasing popularity of using online auctions has led to an increase in fraudulent activity.[2]
                This is usually performed on an auction website by creating a very appetising auction, such as a low
                starting amount. Once a buyer wins an auction and pays for it, the fradulent seller will either not
                pursue with the delivery,[17] or send a less valuable version of the purchased item (replicated, used,
                refurbished, etc.). Protection to prevent such acts has become readily available, most notably Paypal's
                buyer protection policy. As Paypal handles the transaction, they have the ability to hold funds until a
                conclusion is drawn whereby the victim can be compensated.
            </p>
            <h5>Sale of stolen goods</h5>
            <p>
                Online auction websites are used by thieves or fences to sell stolen goods to unsuspecting buyers.[19]
                According to police statistics there were over 8000 crimes involving stolen goods, fraud or deception
                reported on eBay in 2009.[20] It has become common practice for organised criminals to steal in-demand
                items, often in bulk. These items are then sold online as it is a safer option due to the anonymity and
                worldwide market it provides.[21] Auction fraud makes up a large percentage of complaints received by
                the FBI’s Internet Crime Complaint Center (IC3). This was around 45% in 2006 and 63% in 2005
            </p>
            <h3>Bidding techniques</h3>
            <h5>Auction sniping</h5>
            <p>
                Auction sniping is a controversial bidding technique used in timed online auctions.[23] It is the
                practice of placing a bid in the final stages of an auction with the aim of removing other bidder's
                ability to place another bid before the auction ends. These bids can either be placed by the bidder
                manually or automatically with the use of a tool.[24] There are tools available that have been developed
                for this purpose. However, the use of these tools is the subject of much controversy.[25]

                There are two different approaches employed by sniping tools.
            <ul>
                <li>
                    Online: These are hosted on a remote server and are a service run by a third party.
                </li>
                <li>
                    Local: This type is a script which can be downloaded onto the users computer which is then activated
                    and run locally.
                </li>

            </ul>


            </p>
        </div>
        <div class="col-md-2">
            <br>
            <br>
            <br>
            <br>
            <img class="img-fluid rounded" src="{{asset('public/admin/ad/home-delivery-job-interview.jpg')}}"
                 --}}
{{--height="900%" width="400%"--}}{{--
 alt="">
            <br>
            <br>
            <br>
            <br>
            <img class="img-fluid rounded" src="{{asset('public/admin/ad/ok1.jpg')}}"
                 --}}
{{--height="900%" width="400%"--}}{{--
 alt="">
            <br>
            <br>
            <br>
            <br>
            <img class="img-fluid rounded" src="{{asset('public/admin/ad/home-1.png')}}"
                 --}}
{{--height="900%" width="400%"--}}{{--
 alt="">

        </div>

    </div>
</div>

</body>


<script src="{{url('public/admin/js/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/admin/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/admin/js/material.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/admin/js/material.min.js')}}" type="text/javascript"></script>

<!--  PerfectScrollbar Library -->
<script src="{{url('public/admin/js/perfect-scrollbar.jquery.min.js')}}"></script>
<!--  Notifications Plugin    -->
<scrpt src="{{url('public/admin/js/bootstrap-notify.js')}}"></scrpt>

<!-- Material Dashboard javascript methods -->
<script src="{{url('public/admin/js/material-dashboard.js?v=1.2.0')}}}"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{url('public/admin/js/demo.js')}}"></script>


<!--to use jquery or ajax -->
<script src=" {!!asset('public/theme/vendor/jquery/jquery.min.js')!!} " }></script>
<script src="{!!asset('public/theme/vendor/bootstrap/js/bootstrap.bundle.js')!!}" }></script>
<script src="{!!asset('public/theme/vendor/bootstrap/js/bootstrap.js')!!}" }></script>


</html>--}}
