@extends('layouts.buyers_base_layout')
@section('content')

    <div class="wrapper">
        <div class="">
            @include('layouts.partials.buyers_nav')
            @include('layouts.partials.header')

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="flash-message">
                                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                    @if(Session::has('alert-' . $msg))

                                        <p class="alert alert-{{ $msg }}">
                                            @if($msg=='success')
                                                <img src="{{URL::asset('/public/icons/tick.png')}}" height="35px">
                                            @else
                                                <img src="{{URL::asset('/public/icons/cross.png')}}" height="35px">
                                            @endif

                                            <?php echo Session::get('alert-' . $msg); ?> <a href="#"
                                                                                            class="close"
                                                                                            data-dismiss="alert"
                                                                                            aria-label="close">&times;</a>
                                        </p>
                                    @endif
                                @endforeach
                            </div> <!-- end .flash-message -->
                            <div class="panel panel-default panel-table">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col col-xs-6">
                                            <h3 class="panel-title">My Bids</h3>
                                        </div>

                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-striped table-bordered table-list">
                                        <thead>
                                        <tr>
                                            <th class="text-center">SL</th>
                                            <th>Image</th>
                                            <th>Product Name</th>
                                            <th>Biding Amount</th>
                                            <th>Total Bid</th>
                                            <th>Max Bid</th>
                                            <th>Bid Endtime</th>
                                            <th><em class="fa fa-cog"></em></th>
                                            <th><em class="fa fa-edit"></em></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = $bids->perPage() * ($bids->currentPage() - 1);?>
                                        @foreach($bids as $bid)
                                            @php
                                                $product = $bid->product;

                                                $productBids = $product->Bid()->get()->all();
                                                $count = count($productBids);
                                                $max = 0;
                                                foreach ($productBids as $pbid){
                                                 if($pbid->price>$max){
                                                   $max = $pbid->price;
                                                 }
                                                }
                                            @endphp

                                            <tr>
                                                <td class="text-center">{{++$i}}</td>
                                                <td><a href="{{url('buyer/product_desc/'.$product->id)}}">
                                                        <img width="70px" height="70px" src="{{URL::asset('public/'.$product->image1)}}"
                                                        >
                                                    </a>
                                                </td>
                                                <td>{{$product->name}}</td>
                                                <td>{{$bid->price}}</td>
                                                <td>{{$count}}</td>
                                                <td>{{$max}}</td>
                                                <td>{{$product->end_time}}</td>
                                                <td>
                                                    <a href="{{url('buyer/product_desc/'.$product->id)}}"
                                                       class="btn btn-sm">
                                                        View
                                                    </a>
                                                </td>

                                                <td>
                                                    @if(!$bid->awarded)
                                                        <a class="btn btn-success delete-court-button"
                                                           role="button"
                                                           data-action-link="public/placebid/{{$product->id}}"
                                                           data-toggle="modal"
                                                           data-target="#modal-delete">Edit</a>
                                                    @else
                                                        <a class="btn btn-white delete-court-button">Awarded</a>
                                                    @endif
                                                </td>

                                            </tr>
                                            <?php $i++; ?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col col-xs-8">
                                            {{ $bids->Links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            @include('layouts.partials.header')
            @include('layouts.partials.footer')
                </div>

                <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <form id="delete-court-form" method="POST" action="">

                                {{csrf_field()}}

                                <div class="modal-header text-center">
                                    <h4 class="modal-title w-100 font-weight-bold">Place Bid</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body mx-3">
                                    <div class="text-center md-form mb-5 ">


                                        <div class="form-inline">
                                            <label for="price">Price(BDT):</label>
                                            <input type="number" name="price" id="price">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer d-flex justify-content-center">
                                    <input type="submit" value="Bid">
                                </div>


                            </form>

                        </div>

                    </div>

                    <script>
                        $('.delete-court-button').on('click', function () {
                            $('#delete-court-form').attr('action', $(this).data('action-link'));
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
@endsection