@extends('layouts.buyers_base_layout')
@section('content')
    <div class="container-fluid">
        @include('layouts.partials.buyers_nav')
        @include('layouts.partials.header')
        <div class="">
            <div class="">
                <div class="col-md-2">

                </div>
                <div class="col-md-8">

                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">
                                    @if($msg=='success')
                                        <img src="{{URL::asset('/public/icons/tick.png')}}" height="35px">
                                    @else
                                        <img src="{{URL::asset('/public/icons/cross.png')}}" height="35px">
                                    @endif

                                    <?php echo Session::get('alert-' . $msg); ?> <a href="#"
                                                                                    class="close"
                                                                                    data-dismiss="alert"
                                                                                    aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div>

                    <div class="well">
                        <div class="card">


                            @php
                                $bids = \App\Product::find($product->id)->Bid()->get()->all();
                                $max = 0;
                                foreach ($bids as $bid){
                                 if($bid->price>$max){
                                   $max = $bid->price;
                                 }
                                }
                            @endphp

                            <h3><p class="text-center">Product Bid</p></h3>
                            <img src="{{URL::asset('public/'.$product->image1)}}" class="center-block"
                                 style="width: 300px; height: 300px;">
                            <br>
                            <table class="table borderless" style="width:100%">
                                <style>
                                    textarea {
                                        max-width: 100%;
                                        max-height: 100%;
                                        resize: none;
                                    }
                                </style>
                                <tr>
                                    <td>Product Name</td>
                                    <td>:</td>
                                    <td>{{$product->name}}</td>
                                </tr>
                                <tr>
                                    <td>Product Description</td>
                                    <td>:</td>
                                    <td><textarea rows="4" cols="50" class="field left"
                                                  readonly>{{$product->description}}</textarea></td>
                                </tr>
                                <tr>
                                    <td>Category</td>
                                    <td>:</td>
                                    <td>{{$product->category}}</td>
                                </tr>
                                <tr>
                                    <td>Minimum Bid</td>
                                    <td>:</td>
                                    <td>{{$product->price}}</td>
                                </tr>
                                <tr>
                                    <td>Bid Endtime</td>
                                    <td>:</td>
                                    <td>{{$product->end_time}}</td>
                                </tr>
                                <tr>
                                    <td>Total Bid Count</td>
                                    <td>:</td>
                                    <td><a href="{{url('/see_bidders/'.$product->id)}}">{{count($bids)}}</a></td>
                                </tr>
                                <tr>
                                    <td>Max Bid</td>
                                    <td>:</td>
                                    <td>{{$max}}</td>
                                </tr>
                            </table>
                            <div class="container-fluid">
                                <div class="row top-buffer">
                                    <?php
                                    $end_time = new DateTime($product->end_time);
                                    $now = new DateTime();

                                    $bids = $product->bid;
                                    $awarded = false;
                                    $bidded = false;
                                    foreach ($bids as $bid) {
                                        $awarded = $bid->awarded;
                                        if ($bid->user->id == Auth::user()->id) {
                                            $bidded = true;
                                        }

                                    }
                                    $ended = $now > $end_time;
                                    ?>
                                    <a
                                            @if(!$awarded && !$ended)
                                            class="btn btn-success delete-court-button"
                                            data-action-link="{{url('public/placebid/'.$product->id)}}"
                                            data-toggle="modal"
                                            data-target="#modal-delete" ;
                                            @endif;
                                    >
                                        <?php
                                        if ($ended)
                                            echo '&nbsp;&nbsp;&nbsp;&nbsp;Ended&nbsp;&nbsp;&nbsp;';
                                        else if ($awarded)
                                            echo '&nbsp;&nbsp;&nbsp;&nbsp;Awarded&nbsp;&nbsp;&nbsp;';
                                        else if ($bidded)
                                            echo '&nbsp;&nbsp;&nbsp;&nbsp;Bidded&nbsp;&nbsp;&nbsp;';
                                        else
                                            echo '&nbsp;&nbsp;&nbsp;&nbsp;Bid&nbsp;&nbsp;&nbsp;';
                                        ?>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">

                    </div>

                    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <form id="delete-court-form" method="POST" action="">

                                    {{csrf_field()}}

                                    <div class="modal-header text-center">
                                        <h4 class="modal-title w-100 font-weight-bold">Place Bid</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div class="modal-body mx-3">
                                        <div class="text-center md-form mb-5 ">
                                            <div class="form-inline">
                                                <label for="price">Price(BDT):</label>
                                                <input type="number" name="price" id="price">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer d-flex justify-content-center">
                                        <input type="submit" value="Bid">
                                    </div>

                                </form>

                            </div>

                        </div>

                        <script>
                            $('.delete-court-button').on('click', function () {
                                $('#delete-court-form').attr('action', $(this).data('action-link'));
                            });
                        </script>

                    </div>

                </div>
            </div>

            @include('layouts.partials.header')
            @include('layouts.partials.footer')
        </div>
@endsection

