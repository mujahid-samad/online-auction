@extends('layouts.buyers_base_layout')
@section('content')
    <div class="wrapper">
        <div class="">
            @include('layouts.partials.buyers_nav')
            @include('layouts.partials.header')
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">
                                    @if($msg=='success')
                                        <img src="{{URL::asset('/public/icons/tick.png')}}" height="35px">
                                    @else
                                        <img src="{{URL::asset('/public/icons/cross.png')}}" height="35px">
                                    @endif

                                    <?php echo Session::get('alert-' . $msg); ?> <a href="#"
                                                                                    class="close"
                                                                                    data-dismiss="alert"
                                                                                    aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div> <!-- end .flash-message -->

                    <div class="panel panel-default panel-table">
                        <div class="panel-heading">

                            <div class="row">
                                <div class="col col-xs-2">
                                    <h3 class="panel-title">All Products</h3>
                                </div>
                                <div class="col col-xs-8">
                                    Category :
                                    <select id="mySelect" onchange="myFunction()">
                                        <?php
                                        if (isset($_GET['category'])) {
                                            $category = $_GET['category'];
                                        } else {
                                            $category = "all";
                                        }

                                        $categories = \App\Category::select('name')->get()->all();
                                        $categories = array_column($categories, 'name');
                                        array_push($categories, 'all');
                                        ?>

                                        @foreach ($categories as $c)

                                            @if($c == $category)

                                                <option value="<?php echo $c;?>"
                                                        selected="selected"><?php echo $c;?></option>
                                            @else
                                                <option value="<?php echo $c;?>"><?php echo $c;?></option>
                                            @endif
                                        @endforeach

                                    </select>

                                    <?php
                                    if ($category == 'all') {
                                        $products = App\Product::orderBy('created_at', 'desc')->paginate(10);
                                    } else {
                                        $products = App\Product::where('category', $category)->orderBy('created_at', 'desc')->paginate(10);
                                    }
                                    ?>

                                    <script>
                                        function myFunction() {
                                            var x = document.getElementById("mySelect").value;
                                            window.location.href = '?category=' + x;
                                        }

                                    </script>

                                </div>

                            </div>
                        </div>

                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-list">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th>Image</th>
                                    <th>Category</th>
                                    <th>Product Name</th>
                                    <th>Minimum Bid</th>
                                    <th>Bid Endtime</th>
                                    <th><em class="fa fa-cog"></em></th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = $products->perPage() * ($products->currentPage() - 1);?>
                                @foreach($products as $product)
                                    <tr>
                                        <td class="text-center">{{++$i}}</td>

                                        <td><a href="{{url('buyer/product_desc/'.$product->id)}}">
                                                <img width="70px" height="70px"
                                                     src="{{URL::asset('public/'.$product->image1)}}"
                                                >
                                            </a>
                                        </td>

                                        <td>{{$product->category}}</td>
                                        <td>{{$product->name}}</td>
                                        <td>{{$product->price}}</td>
                                        <td>{{$product->end_time}}</td>

                                        <td>
                                            <a href="{{url('buyer/product_desc/'.$product->id)}}"
                                               class="btn btn-sm">
                                                View
                                            </a>
                                        </td>

                                        <td>
                                            <?php
                                            $end_time = new DateTime($product->end_time);
                                            $now = new DateTime();

                                            $bids = $product->bid;
                                            $awarded = false;
                                            $bidded = false;
                                            foreach ($bids as $bid) {
                                                $awarded = $bid->awarded;
                                                if ($bid->user->id == Auth::user()->id) {
                                                    $bidded = true;
                                                }
                                            }
                                            $ended = $now > $end_time;
                                            ?>
                                            <a
                                                    @if(!$awarded && !$ended)
                                                    class="btn btn-success delete-court-button"
                                                    data-action-link="{{url('public/placebid/'.$product->id)}}"
                                                    data-toggle="modal"
                                                    data-target="#modal-delete" ;
                                                    @endif;
                                            >
                                                <?php
                                                if ($ended)
                                                    echo '&nbsp;&nbsp;&nbsp;&nbsp;Ended&nbsp;&nbsp;&nbsp;';
                                                else if ($awarded)
                                                    echo '&nbsp;&nbsp;&nbsp;&nbsp;Awarded&nbsp;&nbsp;&nbsp;';
                                                else if ($bidded)
                                                    echo '&nbsp;&nbsp;&nbsp;&nbsp;Bidded&nbsp;&nbsp;&nbsp;';
                                                else
                                                    echo '&nbsp;&nbsp;&nbsp;&nbsp;Bid&nbsp;&nbsp;&nbsp;';
                                                ?>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col col-xs-8">
                                    {{ $products->Links() }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            @include('layouts.partials.header')
            @include('layouts.partials.footer')
        </div>

        <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <form id="delete-court-form" method="POST" action="">

                        {{csrf_field()}}

                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold">Place Bid</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body mx-3">
                            <div class="text-center md-form mb-5 ">
                                <div class="form-inline">
                                    <label for="price">Price(BDT):</label>
                                    <input type="number" name="price" id="price">
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer d-flex justify-content-center">
                            <input type="submit" value="Bid">
                        </div>

                    </form>

                </div>

            </div>

            <script>
                $('.delete-court-button').on('click', function () {
                    $('#delete-court-form').attr('action', $(this).data('action-link'));
                });
            </script>

        </div>

    </div>
    <script>
        $(".alert-dismissable").fadeTo(1000, 500).slideUp(500, function () {
            $(".alert-dismissable").alert('close');
        });
    </script>

@endsection