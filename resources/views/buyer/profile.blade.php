@extends('layouts.buyers_base_layout')
@section('content')
    <div class="">
        <div class="container-fluid">
            {{-- <div class="main-panel">--}}
            @include('layouts.partials.buyers_nav')
            @include('layouts.partials.header')
            <div class="">
                <div class="row">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-6">
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))

                                    <p class="alert alert-{{ $msg }}">
                                        @if($msg=='success')
                                            <img src="{{URL::asset('/public/icons/tick.png')}}" height="35px">
                                        @else
                                            <img src="{{URL::asset('/public/icons/cross.png')}}" height="35px">
                                        @endif

                                        <?php echo Session::get('alert-' . $msg); ?> <a href="#"
                                                                                        class="close"
                                                                                        data-dismiss="alert"
                                                                                        aria-label="close">&times;</a>
                                    </p>
                                @endif
                            @endforeach
                        </div> <!-- end .flash-message -->
                        <div class="card">
                            <div class="card-header" data-background-color="blue">
                                <h4 class="text-center">Profile</h4>
                            </div>
                            <div class="card-content">
                                <form method="post" action="{{url('profile/'.$user->id)}}">
                                    {{ csrf_field() }}


                                    <img src="{{URL::asset('public/'.$user->image)}}"
                                         class="center-block img-circle"
                                         style="width: 200px; height: 200px;">
                                    <br>
                                    <table class="table borderless" style="width:100%">
                                        <style>
                                            textarea {
                                                max-width: 100%;
                                                max-height: 100%;
                                                resize: none;
                                            }
                                        </style>
                                        <tr>
                                            <td>Name</td>
                                            <td>:</td>
                                            <td>{{$user->name }}</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>:</td>
                                            <td>{{ $user->email }}</td>
                                        </tr>
                                        <tr>
                                            <td>Phone</td>
                                            <td>:</td>
                                            <td>{{$user->phone }}</td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td>:</td>
                                            <td>{{$user->address }}</td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>

                </div>
            </div>
            @include('layouts.partials.header')
            @include('layouts.partials.footer')

        </div>
    </div>
@endsection
