@extends('login_baselayout')
@section('content')
    <div class="container">
        <div class="card card-register mx-auto mt-5">
            <div class="card-header">Log In As Admin</div>
            <div class="card-body">

                <form class="navbar-form navbar-right" method="POST" action="{{ route('webadmin_login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div>
                            <label for="username">Username</label>
                            <input id="username" type="text" class="form-control" name="username"
                                   value="{{ old('username') }}"
                                   required>

                            @if ($errors->has('username'))
                                <span class="help-block">
                                        <strong style="color:rosybrown">{{ $errors->first('username') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                        <div>
                            <label for="password">Password</label>
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="has-error">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">
                                Login
                            </button>
                        </div>
                    </div>

                </form>

                <div class="text-right">
                    <br>
                    <a class="btn btn-info" href="{{route('login')}}"> Close</a>
                </div>

            </div>
        </div>
    </div>
@endsection

