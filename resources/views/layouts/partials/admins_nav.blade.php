<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container-fluid">

        <?php $user  = Auth::guard('admin')->user();?>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"> </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li style="color: gray">
                    <mark><span style="color: black;"> {{$user->name}}</span> &nbsp; logged in as admin</mark>
                </li>
                <li>
                    <a href="{{ route('logout') }}" class="btn btn-sm" data-toggle="dropdown" onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                        (Logout)
                    </a>
                    <form id="logout-form" action="{{ route('webadmin_logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>

        </div>
    </div>
</nav>