<!-- Navigation -->-
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Online Auction System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{'home'}}">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{'about'}}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#loginModal" data-toggle="modal" data-target="#loginModal">Log-in</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('register')}}" >Register</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

@if(isset($_GET['email_error'])||isset($_GET['password_error']))
    <script>
        $(function () {
            $('#loginModal').modal('show');
        });
    </script>
@endif



<div class="modal fade" id="loginModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Log-in</h4>
            </div>
            <div class="modal-body">

                <form class="navbar-form navbar-right" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group">

                        <div>
                            <label for="email">Email</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   required autofocus>

                            @if (isset($_GET['email_error']))
                                <span class="has-error">
                                        <strong>{{ $_GET['email_error'] }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ isset($_GET['password_error']) ? ' has-error' : '' }}">

                        <div>
                            <label for="password">Password</label>
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if (isset($_GET['password_error']))
                                <span class="has-error">
                                        <strong>{{ $_GET['password_error'] }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="">
                            <button type="submit" class="btn btn-primary">
                                Login
                            </button>
                        </div>
                    </div>

                </form>

                <div class="form-group">
                    <div class="">
                        <a class="btn btn-link" href="{{ route('register') }}">
                            <strong>Register </strong>
                        </a>
                    </div>
                    <div class="">
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            <strong>Forgot Password? </strong>
                        </a>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-info" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script>

    /* $(function () {

         $('#loginModal').on('hide.bs.modal', function (e) {
           // alert('time to show modal');
             $('#loginModal').modal('show')
         })
     });
 */
    $(document).ready(function () {

        // process the form
        $('form').submit(function (event) {

            event.preventDefault();

            console.log('Sending request to ' + $(this).attr('action') + ' with data: ' + $(this).serialize());

            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'email': $('input[name=email]').val(),
                'password': $('input[name=password]').val(),

            };

            // process the form
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                },
                type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url: $(this).attr('action'), // the url where we want to POST
                data: $(this).serialize(), // our data object

                // what type of data do we expect back from the server

               success: function (response) {
                    console.log("Success");
                    $(this).modal('hide');
                    
                    var getUrl = window.location;
                    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
                    
                    console.log('baseurl:'+baseUrl);
                 
                     window.location =baseUrl+'/home';
                    //window.location = '/home';

                },
                error: function (jqXHR) {
                    var response = $.parseJSON(jqXHR.responseText);
                    console.log("Error");
                    if (response.email) {

                        //$('#loginModal').modal('hide')
                        window.location = '?email_error=' + response.email;

                    }
                    else if (response.password) {
                        window.location = '?password_error=' + response.password;
                    }
                    else{
                        console.log("Error");
                    }


                }
            });


        });
    })


    /* $(document).ready(function () {

         // process the form
         $('form').submit(function (event) {

             event.preventDefault();

             console.log('Sending request to ' + $(this).attr('action') + ' with data: ' + $(this).serialize());

             // get the form data
             // there are many ways to get this data using jQuery (you can use the class or id also)
             var formData = {
                 'email': $('input[name=email]').val(),
                 'password': $('input[name=password]').val(),

             };

             // process the form
             $.ajax({
                 headers: {
                     'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                 },
                 type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                 url: $(this).attr('action'), // the url where we want to POST
                 data:$(this).serialize() // our data object

                 // what type of data do we expect back from the server

             })
             // using the done promise callback
                 .done(function (data) {

                     // log data to the console so we can see
                     console.log(data);

                     // here we will handle errors and validation messages
                     if (!data.success) {

                         // handle errors for email ---------------
                         /!*if (data.errors.email) {
                             $('#email-group').addClass('has-error'); // add the error class to show red input
                             $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                         }
 *!/

                     } else {
                         // ALL GOOD! just show the success message!
                         $('form').append('<div class="alert alert-success">' + data.message + '</div>');

                         // usually after form submission, you'll want to redirect
                         // window.location = '/thank-you'; // redirect a user to another page
                         alert('success'); // for now we'll just alert the user
                     }
                 })
                 .fail( function(xhr, textStatus,error) {
                     console.log("Error");
                     console.log(error);
                 });



         });

     });
 */

</script>







