<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Report') }}</title>

<!-- Styles -->

<link rel="apple-touch-icon" sizes="76x76" href="{{asset('public/img/apple-icon.png')}}" />
<link rel="icon" type="image/png" href="{{asset('public/img/favicon.png')}}" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Report</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />
<!-- Bootstrap core CSS     -->
<link href="{{asset('public/admin/css/bootstrap.min.css')}}" rel="stylesheet" />
<!--  Material Dashboard CSS    -->
<link href="{{asset('public/admin/css/material-dashboard.css?v=1.2.0')}}" rel="stylesheet" />
<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="{{asset('public/admin/css/demo.css')}}" rel="stylesheet" />


<!--     Fonts and icons     -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>





