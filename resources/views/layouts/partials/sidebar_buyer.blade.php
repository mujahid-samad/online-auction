<div class="sidebar" data-color="blue" data-image="admin/img/sidebar-1.jpg">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="logo">
        <a href="{{url('/home')}}" class="btn btn-dark">
            {{env('APP_NAME')}}
        </a>
    </div>
    <div class=" sidebar-wrapper">
        <ul class="nav">

            <li {{ (Request::is('home') ? 'class=active' : '') }}>
                <a class="btn btn-dark" href="{{url('/home')}}">
                    <i class="material-icons">library_books</i>
                    <p>Home</p>
                </a>
            </li>

            <li {{ (Request::is('products') ? 'class=active' : '') }}>
                <a class="btn btn-dark" href="{{url('/products')}}">
                    <i class="material-icons">library_books</i>
                    <p>All Products</p>
                </a>
            </li>

            <li {{ (Request::is('mybids') ? 'class=active' : '') }}>
                <a class="btn btn-dark" href="{{url('/mybids')}}">
                    <i class="material-icons">view_list</i>
                    <p>My Bids</p>
                </a>
            </li>


        </ul>
    </div>
</div>