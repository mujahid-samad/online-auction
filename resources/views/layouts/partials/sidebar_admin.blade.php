<div class="sidebar" data-color="blue" data-image="admin/img/sidebar-1.jpg">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="logo">
        <a href="{{url('/home')}}"class="btn btn-dark">
            {{env('APP_NAME')}}
        </a>
    </div>

    <?php $user = auth()->guard('admin')->user();?>

    <div class=" sidebar-wrapper">
        <ul class="nav">

            <li {{ (Request::is('home') ? 'class=active' : '') }}>
                <a class="btn btn-dark" href="{{url('webadmin/home')}}">
                    <i class="material-icons">library_books</i>
                    <p>Home</p>
                </a>
            </li>

            <li {{ (Request::is('/webadmin/pending_products') ? 'class=active' : '') }}>
                <a class="btn btn-dark" href="{{url('/webadmin/pending_products')}}">
                    <i class="material-icons">view_list</i>
                    <p>Pending Posts</p>
                </a>
            </li>


            <li {{ (Request::is('/webadmin/products') ? 'class=active' : '') }}>
                <a  class="btn btn-dark" href="{{url('/webadmin/products')}}">
                    <i class="material-icons">view_list</i>
                    <p>All Posts</p>
                </a>
            </li>


            <li {{ (Request::is('/webadmin/add_category') ? 'class=active' : '') }}>
                <a class="btn btn-dark" href="{{url('/webadmin/add_category')}}">
                    <i class="material-icons">view_list</i>
                    <p>Add Category</p>
                </a>
            </li>

            <li {{ (Request::is('/webadmin/categories') ? 'class=active' : '') }}>
                <a class="btn btn-dark" href="{{url('/webadmin/categories')}}">
                    <i class="material-icons">view_list</i>
                    <p>See Categories</p>
                </a>
            </li>

        </ul>
    </div>
</div>