<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.partials.admin-head')
</head>
<style>
    /*.view_parent_image1 {
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: url(
    {{ URL::asset('public/admin/img/Picture1.png')}}
    ) no-repeat center center fixed;
            background-size: cover;
        }*/
</style>
<body>
{{--<div id="main-wrapper">--}}
{{--<div class='view_parent_image1'>--}}
<div class="main-panel">
    @include('layouts.partials.sidebar_buyer')
    <div class="page-wrapper">
        @yield('content')
    </div>
</div>
{{--</div>--}}
@include('layouts.partials.admin-footer-script')
@include('layouts.partials.footer-scripts')

</body>
</html>