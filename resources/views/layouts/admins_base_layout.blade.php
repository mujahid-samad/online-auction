<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.partials.admin-head')
</head>
<style>
    .view_parent_image1 {
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: url({{ URL::asset('public/admin/img/background-transparent-hd-2.jpg')}}) no-repeat center center fixed;
        background-size: cover;
    }
</style>
<body>
{{--<div class='view_parent_image1'>--}}
<div class="main-panel">
    @include('layouts.partials.sidebar_admin')
    <div class="page-wrapper">
        @yield('content')
    </div>
</div>
{{--</div>--}}
@include('layouts.partials.admin-footer-script')

</body>
</html>