<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Small Business - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href= "{!!asset('public/theme/vendor/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">

    <!--to use jquery or ajax -->
    <script src=" {!!asset('public/theme/vendor/jquery/jquery.min.js')!!} " }></script>
    <script src="{!!asset('public/theme/vendor/bootstrap/js/bootstrap.bundle.js')!!}" }></script>
    <script src="{!!asset('public/theme/vendor/bootstrap/js/bootstrap.js')!!}" }></script>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>


</head>


<body class="bg-dark">

@yield('content')


<script src="{{url('public/admin/js/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
<script src ="{{url('public/admin/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/admin/js/material.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/admin/js/material.min.js')}}" type="text/javascript"></script>

<!--  PerfectScrollbar Library -->
<script src="{{url('public/admin/js/perfect-scrollbar.jquery.min.js')}}"></script>
<!--  Notifications Plugin    -->
<scrpt src="{{url('public/admin/js/bootstrap-notify.js')}}"></scrpt>

<!-- Material Dashboard javascript methods -->
<script src="{{url('public/admin/js/material-dashboard.js?v=1.2.0')}}}"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{url('public/admin/js/demo.js')}}"></script>


<!--to use jquery or ajax -->
<script src=" {!!asset('public/theme/vendor/jquery/jquery.min.js')!!} " }></script>
<script src="{!!asset('public/theme/vendor/bootstrap/js/bootstrap.bundle.js')!!}" }></script>
<script src="{!!asset('public/theme/vendor/bootstrap/js/bootstrap.js')!!}" }></script>

</body>
</html>