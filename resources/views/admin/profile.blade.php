@extends('layouts.admins_base_layout')
@section('content')
    <div class="container-fluid">
        <div class="wrapper">
            <div class="main-panel">
                @include('layouts.partials.admins_nav')
                @include('layouts.partials.header')
                <div class="row">

                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">
                                    @if($msg=='success')
                                        <img src="{{URL::asset('/public/icons/tick.png')}}" height="35px">
                                    @else
                                        <img src="{{URL::asset('/public/icons/cross.png')}}" height="35px">
                                    @endif

                                    <?php echo Session::get('alert-' . $msg); ?> <a href="#"
                                                                                    class="close"
                                                                                    data-dismiss="alert"
                                                                                    aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div> <!-- end .flash-message -->


                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header" data-background-color="blue">
                                <h4 class="text-center">Profile</h4>
                            </div>
                            <div class="card-content">
                                {{-- <form method="post" action="{{url('profile/'.$user->id)}}">
                                     {{ csrf_field() }}--}}
                                <img src="{{URL::asset('public/admin/img/faces/marc.jpg')}}" class="center-block"
                                     style="width: 300px; height: 300px;">
                                <br>
                                <table class="table borderless" style="width:100%">
                                    <style>
                                        textarea {
                                            max-width: 100%;
                                            max-height: 100%;
                                            resize: none;
                                        }
                                    </style>
                                    <tr>
                                        <td>Name</td>
                                        <td>:</td>
                                        <td>{{$user->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Username</td>
                                        <td>:</td>
                                        <td>{{$user->username }}</td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td>:</td>
                                        <td>{{$user->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td>:</td>
                                        <td>Dhaka</td>
                                    </tr>

                                </table>

                                {{--</form>--}}
                            </div>
                        </div>
                    </div>

                    {{--<div class="col-md-4">
                        <div class="card card-profile">
                            <div class="card-avatar">
                                <a href="#pablo">
                                    <img class="img" src="admin/img/faces/marc.jpg" />
                                </a>
                            </div>
                            <div class="content">
                                <h6 class="category text-gray">Super Admin</h6>
                                <h4 class="card-title">Follower of Prophet Muhammad</h4>
                                <p class="card-content">
                                    Don't be scared of the truth because we need to restart the human foundation in truth ....
                                    <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                            </div>
                        </div>
                    </div>--}}
                </div>
                {{--</div>
            </div>--}}

                {{--<footer class="footer">
                    <div class="container-fluid">
                        <p class="copyright text-center">
                            &copy;
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                            <a href="#"> Report</a>
                        </p>
                    </div>
                </footer>--}}
                @include('layouts.partials.footer')
            </div>
        </div>
    </div>
@endsection
