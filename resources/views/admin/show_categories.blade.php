@extends('layouts.admins_base_layout')
@section('content')
    <div class="">
        <div class="container-fluid">
            {{--<div class="main-panel">--}}
            @include('layouts.partials.admins_nav')
            @include('layouts.partials.header')
            <div class="row">

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))

                                    <p class="alert alert-{{ $msg }}">
                                        @if($msg=='success')
                                            <img src="{{URL::asset('/public/icons/tick.png')}}" height="35px">
                                        @else
                                            <img src="{{URL::asset('/public/icons/cross.png')}}" height="35px">
                                        @endif

                                        <?php echo Session::get('alert-' . $msg); ?> <a href="#"
                                                                                        class="close"
                                                                                        data-dismiss="alert"
                                                                                        aria-label="close">&times;</a>
                                    </p>
                                @endif
                            @endforeach
                        </div> <!-- end .flash-message -->
                        <div class="panel panel-default panel-table">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col col-xs-6">
                                        <h3 class="panel-title">Categories</h3>
                                    </div>
                                    <div class="col col-xs-6 text-right">
                                        <a href="{{ url('webadmin/add_category')}}">
                                            <button type="button" class="btn btn-sm btn-primary btn-create">Create
                                                New
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered table-list">
                                    <thead>
                                    <tr>
                                        <th class="text-center">SL</th>
                                        <th>Category Name</th>
                                        <th><em class="fa fa-cog"></em></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0; ?>
                                    @foreach($categories as $category)
                                        <tr>
                                            <td class="text-center">{{++$i}}</td>
                                            <td>{{$category->name}}</td>
                                            <td align="center">
                                                <a onclick="return confirm('Are you sure to remove this category?')"
                                                   href="{{url('webadmin/delete_category/'.$category->id)}}"
                                                   class="btn btn-sm"><em class="fa fa-trash"></em></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    {{--<div class="col col-xs-4">Page 1 of 5
                                    </div>
                                    <div class="col col-xs-8">
                                        <ul class="pagination hidden-xs pull-right">
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                        </ul>
                                        <ul class="pagination visible-xs pull-right">
                                            <li><a href="#">«</a></li>
                                            <li><a href="#">»</a></li>
                                        </ul>
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--</div>
        </div>
    </div>--}}

            <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <form id="delete-court-form" method="POST" action="">

                            {{csrf_field()}}

                            <div class="modal-header text-center">
                                <h4 class="modal-title w-100 font-weight-bold">Place Bid</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-body mx-3">
                                <div class="text-center md-form mb-5 ">


                                    <div class="form-inline">
                                        <label for="price">Price(BDT):</label>
                                        <input type="number" name="price" id="price">
                                    </div>

                                </div>
                            </div>

                            <div class="modal-footer d-flex justify-content-center">
                                <input type="submit" value="Bid">
                            </div>


                        </form>

                    </div>

                </div>

                <script>
                    $('.delete-court-button').on('click', function () {
                        $('#delete-court-form').attr('action', $(this).data('action-link'));
                    });
                </script>
            </div>
            @include('layouts.partials.header')
            @include('layouts.partials.footer')
        </div>
    </div>
    {{-- </div>--}}


@endsection