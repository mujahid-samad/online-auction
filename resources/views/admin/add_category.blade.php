@extends('layouts.admins_base_layout')
@section('content')
    <div class="">
        <div class="container-fluid">

            {{-- <div class="main-panel">--}}
            @include('layouts.partials.admins_nav')
            @include('layouts.partials.header')
            <div class="row">

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))

                                    <p class="alert alert-{{ $msg }}"><img
                                                src="{{URL::asset('/public/icons/tick.png')}}"
                                                height="35px"> <?php echo Session::get('alert-' . $msg); ?>
                                        <a href="#"
                                           class="close"
                                           data-dismiss="alert"
                                           aria-label="close">&times;</a>
                                    </p>
                                @endif
                            @endforeach
                        </div> <!-- end .flash-message -->
                        <div class="panel panel-default panel-table">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col col-xs-6">
                                        <h3 class="panel-title">Add Category</h3>
                                    </div>

                                </div>
                            </div>
                            <div class="panel-body">
                                <form method="post" action="category">

                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} ">
                                        <label for="name">Category Name</label>
                                        <input class="form-control" id="name" name="name"
                                               value="{{old('name')}}"
                                               type="text"
                                               aria-describedby="nameHelp"
                                        required autofocus>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <button class="btn btn-primary btn-block">Submit</button>


                                </form>

                            </div>
                            <div class="panel-footer">
                                <div class="row">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('layouts.partials.header')
            @include('layouts.partials.footer')
        </div>
    </div>
    {{-- </div>--}}



@endsection