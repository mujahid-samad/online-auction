<?php

namespace App;

use App\Http\Controllers\BuyersHome;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Product extends Model
{
    protected $guarded = ['created_at','updated_at'];

    public function User(){
        return $this->belongsTo(User::class);
    }

    public function Bid(){
        return $this->hasMany(Bid::class);
    }

    public $rules = [
        'name' => 'required|string|max:255',
        'price' => 'required|numeric',
        'description' => 'required|string',
        'image1' => 'max:500000',
    ];

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);
        return $v->validate();
    }

    public function delete()
    {
        if(isset($this->image1)) {

            if (file_exists('public/' . $this->image1)) {
                @unlink('public/' . $this->image1);
            }

        }
        parent::delete();
    }


}
