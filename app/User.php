<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'updated_at', 'created_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Product(){

        return $this->hasMany(Product::Class);

    }

    public function PendingProduct(){

        return $this->hasMany(PendingProduct::Class);

    }


    public function BiddedProducts(){

        return $this->belongsTo(Product::class,Bid::class);

    }

    public function Bid(){

        return $this->hasMany(Bid::Class);

    }

    public function delete()
    {
        if(isset($this->image)) {

            if (file_exists('public/' . $this->image)) {
                @unlink('public/' . $this->image);
            }

        }
        parent::delete();
    }




}
