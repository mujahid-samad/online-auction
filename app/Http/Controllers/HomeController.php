<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     *  redirect user to their own homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if($user->user_type == 'buyer'){
           // dd('buyer');
            return redirect()->route('buyers_home');
        }
        else if($user->user_type == 'seller') {
           // dd('seller');
            return redirect()->route('sellers_home');
        }
        else if($user->user_type == 'admin') {
           return  redirect()->route('admins_home');
        }
        else {
            echo 'User type not matched with any predefined';
        }
    }


}
