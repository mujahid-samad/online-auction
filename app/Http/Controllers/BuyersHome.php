<?php

namespace App\Http\Controllers;

use App\Bid;
use App\Product;
use DateTime;
use Illuminate\Http\Request;

use Auth;

class BuyersHome extends Controller
{

    /**
     *  redirect user to their own homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('buyers_profile');
    }

    public function profile()
    {
        $user = Auth::user();
        return view('buyer.profile')->with('user',$user);
    }

    public function  products()
    {
        $products = Product::orderBy('created_at', 'desc')->paginate(10);
        return view('buyer.all_products')->with('products', $products);
    }

    public function  mybids(){
        $user = Auth::user();
        $bids = $user->Bid()->orderBy('created_at', 'desc')->paginate(10);
        return view('buyer.my_bids')->with('bids', $bids);
    }

    public function placebid(Request $request,$product_id){

        $user = Auth::user();
        $product = \App\Product::find($product_id);
        $bids = $product->Bid()->get()->all();
        $max = 0;
        foreach ($bids as $bid){
            if($bid->price>$max){
                $max = $bid->price;
            }
        }



        $end_time = new DateTime($product->end_time);
        $now = new DateTime();

        if($now>$end_time){
            $request->session()->flash('alert-danger', 'Project bidding time is expired');
            return back();
        }

        if($request->price < $product->price ){

            $request->session()->flash('alert-danger', 'Minimum bid amount : '.$product->price);
            return back();
        }



        if($request->price < $max){

            $request->session()->flash('alert-danger', 'Bid amount must be greater than current max: '.$max);
            return back();
        }

        $existed = $user->Bid()->where('product_id',$product_id)->get()->first();

        if($existed){
            $bid = $existed;
            $bid->price = $request->price;
            $bid->save();
            $request->session()->flash('alert-success', 'Bidded successfully');
            return back();
        } else {
            $bid = new Bid();
            $bid->price = $request->price;
            $bid->product_id = $product_id;
            $user->Bid()->save($bid);
        }

        $request->session()->flash('alert-success', 'Bidded successfully');
        return back();
    }

}
