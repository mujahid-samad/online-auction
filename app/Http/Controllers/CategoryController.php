<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    //
    public function index(){
        return view('admin.show_categories')->with('categories',Category::all());
    }

    public function create(){
        return view('admin.add_category');
    }

    public function store(Request $request){
        $category = new Category($request->all());
        $category->save();
        $request->session()->flash('alert-success', 'Category Created successfully');
        return redirect()->back();

    }

    public function update(){

    }

    public function delete(Request $request,$id){
        $category = Category::find($id);
        $category->delete();
        $request->session()->flash('alert-success', 'Deleted successfully');
        return redirect()->back();
    }

}
