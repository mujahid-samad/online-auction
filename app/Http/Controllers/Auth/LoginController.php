<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
      /// protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

        $this->middleware('guest')->except('logout');
    }


    protected function sendFailedLoginResponse(Request $request)
    {

        if (!User::where('email', $request->email)->first()) {


            $message = 'Invalid Email';

            return response()->json(['email' => $message], 422);
        }

        if (!User::where('email', $request->email)->where('password', bcrypt($request->password))->first()) {

            $message = 'Wrong Password';

            return response()->json(['password' => $message], 422);
        }

    }

    protected function authenticated(Request $request, $user)
    {
      
        return redirect()->route('home');

    }


}
