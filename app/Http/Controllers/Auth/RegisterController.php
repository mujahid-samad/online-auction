<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = '/home';

    protected function validator(array $data)
    {
      //  dd($data);
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'image' => 'max:500000',
            'password' => 'required|string|min:6|confirmed',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {


       /* $user = User::create([
            'name' => $request['name'],
            'user_type' => $request['user_type'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'password' => bcrypt($request['password']),

            ]);*/

        $request->merge([ 'password' => bcrypt($request->password) ]);

        $user = User::create(collect($request)->except('image','password_confirmation')->all());

        if ($request->hasFile('image')) {
            $imageName = $request->file('image')->getClientOriginalName();
            $target = $request->file('image')->move(public_path('avatars'), $imageName);
            $user->image = 'avatars/' . $imageName;
        }

        $user->save();
        return $user;
    }

    /*protected function registered(Request $request, $user)
    {
        //$user->generateToken();//

        return response()->json(['data' => "success"], 201);
    }*/
}
