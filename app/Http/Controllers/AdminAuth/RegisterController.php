<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Support\Facades\Input;
use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{


    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:admins',
            'password' => 'required|string|min:6',
        ]);

    }

    public function response(array $errors)
    {
        return response()->json(['errors' => $errors], 400);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create()
    {

        $data = Input::all();
       // $this->validator($data)->validate();  // if not validate will rise an exception

       // dd("validated");

        $admin = Admin::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => isset($data['email'])?$data['email']:null,
            'password' => bcrypt($data['password']),
        ]);
        return $admin;
    }


}
