<?php

namespace App\Http\Controllers\AdminAuth;

use App\Http\Controllers\Controller;

use app\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Input;


use Illuminate\Support\Facades\Lang;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    /// protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

       // $this->middleware('guest')->except('logout');

        //$this->middleware('guest')->except('logout'); //will give error because we did login inside on api route
    }

    public function index()
    {
        return view('admin_auth.login');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    private function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|string',
            'password' => 'required|string|min:4',
        ]);
    }

    protected function login(Request $request)
    {


        $validator = $this->validator($request->all());  // if not validate will rise an exception

        if($validator->fails()){
            //dd($validator->errors());
            return redirect()->back()->withErrors($validator->errors());;
        }

        if (Auth::guard('admin')->attempt($request->only('username', 'password'), true)) {


            $request->session()->regenerate();
            $admin = Auth::guard('admin')->user();

            return  redirect()->route('webadmin_home');

        } else {

            //print_r($admin);

            return redirect()->back()->withErrors(['Error '=> Lang::get('auth.email'),
            ]);

        }

    }


    protected function sendFailedLoginResponse(Request $request)
    {

        if (!Admin::where('username', $request->username)->first()) {
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    $this->username() => Lang::get('auth.email'),
                ]);
        }

        if (!Admin::where('username', $request->username)->where('password', bcrypt($request->password))->first()) {
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    'password' => Lang::get('auth.password'),
                ]);
        }

    }


    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return response()->json(['data' => $user], 200);
    }


    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        Session::flush();
        $request->session()->regenerate();
        return redirect()->route('webadmin_login');
    }


}
