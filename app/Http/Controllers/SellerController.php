<?php

namespace App\Http\Controllers;

use App\Bid;
use App\Category;
use App\Notifications\AwardNotification;
use App\PendingProduct;
use App\User;
use Illuminate\Http\Request;

use Auth;

use App\Product;

class SellerController extends Controller
{
    /**
     *  redirect user to their own homepage
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return redirect()->route('sellers_profile');
    }


    public function profile()
    {
        $user = Auth::user();
        return view('seller.profile')->with('user',$user);
    }


    public function myproducts()
    {
        $user = auth()->user();
        $products = $user->Product()->orderBy('created_at', 'desc')->paginate(10);

        return view('seller.my_products')->with('products', $products);
    }

    public function myPendingProducts()
    {
        $user = auth()->user();
        $products = $user->PendingProduct()->orderBy('created_at', 'desc')->paginate(10);

        return view('seller.my_pending_products')->with('products', $products);
    }

    public function createSale()
    {
        $user = Auth::user();
        return view('seller.post_product')->with('user', $user)->with('categories',Category::all());
    }

    public function product_desc($id)
    {
        $product = \App\Product::find($id);

        return view('seller.product_desc')->with('product', $product);
    }

    public function pending_product_desc($id)
    {
        $product = \App\PendingProduct::find($id);

        return view('seller.pending_product_desc')->with('product', $product);
    }

    public function storeSale(Request $request)
    {
        $user = auth()->user();

        $product = new PendingProduct();

        $product->validate($request->all());

        $product->fill($request->all());

        if ($request->hasFile('image1')) {
            $imageName = $request->file('image1')->getClientOriginalName();
            $target = $request->file('image1')->move(public_path('product_picture'), $imageName);
            $product->image1 = 'product_picture/' . $imageName;
        }

        $user->PendingProduct()->save($product);

        $request->session()->flash('alert-success', 'Posted successfully');
        return back();
    }

    public function edit_product($id)
    {
        $product = Product::find($id);
        if (!isset($product)) return;
        return view('seller.edit_product')->with('product', $product)->with('categories',Category::all());;
    }

    public function update_product(Request $request, $id)
    {

        $product = Product::find($id);

        if (!isset($product)) {
            $request->session()->flash('alert-danger', 'Product not found in List, May be its not aprroved by admin');
            return back();
        }

        $product->validate($request->all());

        $product->fill($request->all());

        if ($request->hasFile('image1')) {

            $image = $request->file('image1');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $imageName = $request->file('image1')->getClientOriginalName();
            //return base_path() . '/public/uploads';
            $target = $request->file('image1')->move(public_path('product_picture'), $imageName);
            $product->image1 = 'product_picture/' . $imageName;
        }

        $product->save();

        $request->session()->flash('alert-success', 'Posted successfully');

        return back();
    }

    public function delete_product(Request $request, $id)
    {
        $product = Product::find($id);
        if (!isset($product))
            return redirect()->back();
        $product->delete();

        $request->session()->flash('alert-success', 'Deleted successfully');
        return redirect()->back();
    }

    public function delete_pending_product(Request $request,$id)
    {
        $product = PendingProduct::find($id);
        if (!isset($product)) return;
        $product->delete();

        $request->session()->flash('alert-success', 'Deleted successfully');
        return redirect()->back();
    }

    public function see_bidders($id)
    {
        $product = Product::find($id);
        $bids = $product->Bid()->orderBy('price', 'desc')->paginate(10);
        return view('seller.see_bidders', compact('bids'));
    }

    public function award(Request $request,$bid_id){
        $bid = Bid::find($bid_id);
        $bid->awarded = true;

        $user = $bid->user;
        $user->notify(new AwardNotification($user));

        $bid->save();

        $request->session()->flash('alert-success', 'Awarded');
        return back();
    }
}
