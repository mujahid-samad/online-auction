<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    public function User(){
        return $this->belongsTo(User::class);
    }

    public function Product(){

        return $this->belongsTo(Product::Class);
    }
}
