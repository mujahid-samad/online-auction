<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class PendingProduct extends Model
{
    protected $guarded = ['created_at','updated_at'];

    public function User(){
        return $this->belongsTo(User::class);
    }

    public $rules = [
        'name' => 'required|string|max:255',
        'price' => 'required|numeric',
        'description' => 'required|string'
    ];

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);
        return $v->validate();
    }



}
