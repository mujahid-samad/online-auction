-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2018 at 02:33 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `auction`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', NULL, '$2y$10$nhn.Qa2zjTykNH4tVkmGFuzAAY12fFZgDa7zqkbd9lcM7Dk8ycwBS', 'zMvcd0N5KJC30Ucm1msgLilKc374c0gGGU7SJA8AEVhYwfF6A1Jtd70dKoER', '2018-03-19 11:50:03', '2018-03-19 11:50:03');

-- --------------------------------------------------------

--
-- Table structure for table `bids`
--

CREATE TABLE `bids` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `awarded` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bids`
--

INSERT INTO `bids` (`id`, `user_id`, `product_id`, `price`, `awarded`, `created_at`, `updated_at`) VALUES
(9, 17, 22, '12100', 1, '2018-05-07 00:45:05', '2018-05-07 01:02:48'),
(10, 1, 25, '1501', 0, '2018-05-07 01:20:43', '2018-05-07 01:20:43'),
(11, 1, 24, '10001', 0, '2018-05-07 01:20:50', '2018-05-07 01:20:50'),
(12, 1, 40, '30500', 0, '2018-05-07 04:17:42', '2018-05-07 04:17:42'),
(13, 1, 42, '51000', 0, '2018-05-07 04:29:28', '2018-05-07 04:29:28'),
(14, 17, 39, '41000', 1, '2018-05-07 04:30:21', '2018-05-07 04:30:58'),
(15, 1, 47, '2001', 1, '2018-05-07 06:57:40', '2018-05-07 07:00:07'),
(16, 1, 23, '3000', 0, '2018-05-07 06:58:38', '2018-05-07 06:58:38'),
(17, 17, 47, '2002', 0, '2018-05-07 06:59:14', '2018-05-07 06:59:14'),
(18, 1, 51, '2500', 0, '2018-05-07 07:11:15', '2018-05-07 07:11:15'),
(19, 19, 51, '3000', 1, '2018-05-07 07:12:43', '2018-05-07 07:13:19'),
(20, 20, 50, '2500', 1, '2018-05-07 07:25:15', '2018-05-07 07:26:41'),
(21, 20, 47, '2500', 0, '2018-05-07 07:25:39', '2018-05-07 07:25:39'),
(22, 20, 46, '11000', 0, '2018-05-07 07:25:45', '2018-05-07 07:25:45'),
(23, 20, 42, '60000', 0, '2018-05-07 07:25:53', '2018-05-07 07:25:53'),
(24, 20, 38, '2100', 0, '2018-05-07 07:26:02', '2018-05-07 07:26:02'),
(25, 20, 41, '61000', 0, '2018-05-07 07:26:07', '2018-05-07 07:26:07'),
(26, 21, 46, '11000', 0, '2018-05-07 07:29:31', '2018-05-07 07:29:31'),
(27, 21, 47, '2600', 0, '2018-05-07 07:29:40', '2018-05-07 07:29:40'),
(28, 21, 45, '11000', 0, '2018-05-07 07:29:47', '2018-05-07 07:29:47'),
(29, 17, 42, '60001', 0, '2018-05-07 07:35:36', '2018-05-07 07:35:36'),
(30, 16, 42, '62000', 0, '2018-05-07 07:37:52', '2018-05-07 07:37:52'),
(31, 21, 42, '63000', 0, '2018-05-07 07:38:26', '2018-05-07 07:38:26');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(14, 'Electronics', '2018-04-24 12:47:22', '2018-04-24 12:47:22'),
(15, 'Mobile', '2018-04-24 12:47:32', '2018-04-24 12:47:32'),
(16, 'Hardware', '2018-04-24 12:47:46', '2018-04-24 12:47:46'),
(17, 'Computer', '2018-04-24 12:47:54', '2018-04-24 12:47:54'),
(18, 'Babies', '2018-04-24 12:48:20', '2018-04-24 12:48:20'),
(19, 'Household accessories', '2018-04-24 12:48:31', '2018-04-24 12:48:31'),
(20, 'Others', '2018-04-24 12:49:01', '2018-04-24 12:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2018_03_19_060509_create_admins_table', 3),
(12, '2018_03_21_132934_create_categories_table', 5),
(15, '2018_02_16_034007_create_products_table', 6),
(16, '2018_03_16_060757_create_bids_table', 6),
(17, '2018_03_19_055241_create_pending_products_table', 6),
(18, '2014_10_12_000000_create_users_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('a@gmail.com', '$2y$10$BYGhMBaI/0GJZ.TqqSPtAOfQ6f.5DXksFuelz/efM47fa1yxFqn8a', '2018-04-18 12:34:15'),
('s@gmail.com', '$2y$10$sG5//AZic7hXQI0gbA5e6.EgENhQucg9M9h.3l9h/m6U7u4MzHyGO', '2018-05-07 02:34:31');

-- --------------------------------------------------------

--
-- Table structure for table `pending_products`
--

CREATE TABLE `pending_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pending_products`
--

INSERT INTO `pending_products` (`id`, `user_id`, `name`, `category`, `price`, `end_time`, `description`, `image1`, `image2`, `image3`, `image4`, `image5`, `created_at`, `updated_at`) VALUES
(27, 4, 'mac', 'Computer', '100000', '2018-05-08 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/macbook-pro110221210702.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:28:18', '2018-05-07 04:28:18'),
(28, 4, 'vivo', 'Mobile', '10000', '2018-05-08 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/vivo.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:32:18', '2018-05-07 04:32:18'),
(29, 4, 'mobile', 'Mobile', '15000', '2018-05-17 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/s6_55e7cec344023._samsung-galaxy-s6-edge-32-gb-mobile-phone.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:33:03', '2018-05-07 04:33:03'),
(30, 4, 'mobile', 'Mobile', '10000', '2018-05-09 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/global-mobile-phone-sales-forecasted-by-88-countries.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:33:32', '2018-05-07 04:33:32'),
(32, 4, 'sofa', 'Household accessories', '40000', '2018-05-10 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/lr_sof_10141618_bonitasprings_blue_Bonita-Springs-Blue-Sofa.jpeg', NULL, NULL, NULL, NULL, '2018-05-07 04:34:44', '2018-05-07 04:34:44'),
(33, 4, 'toy', 'Babies', '500', '2018-05-09 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/81KuvIFVHXL._SY450_.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:36:08', '2018-05-07 04:36:08'),
(34, 4, 'toy', 'Electronics', '1500', '2018-04-30 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/download.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:36:43', '2018-05-07 04:36:43'),
(35, 4, 'sofa', 'Household accessories', '100000', '2018-05-09 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/images (1).jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:38:27', '2018-05-07 04:38:27'),
(36, 4, 'sofa', 'Household accessories', '50000', '2018-05-08 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/images (2).jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:38:45', '2018-05-07 04:38:45'),
(37, 4, 'sofa', 'Household accessories', '40000', '2018-05-09 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/images (3).jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:38:59', '2018-05-07 04:38:59'),
(38, 4, 'sofa', 'Household accessories', '40000', '2018-05-09 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/images.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:39:18', '2018-05-07 04:39:18'),
(39, 4, 'sofa', 'Household accessories', '40000', '2018-05-16 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/lr_chr_10316924_bellingham_Cindy-Crawford-Home-Bellingham-Cardinal-Sleeper-Chair.jpeg', NULL, NULL, NULL, NULL, '2018-05-07 04:39:37', '2018-05-07 04:39:37'),
(40, 4, 'sofa', 'Household accessories', '10000', '2018-05-10 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/lr_sof_10117946_bellingham_Cindy-Crawford-Home-Bellingham-Cardinal-Sofa.jpeg', NULL, NULL, NULL, NULL, '2018-05-07 04:39:53', '2018-05-07 04:39:53'),
(41, 4, 'sofa', 'Household accessories', '50000', '2018-05-10 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/lr_sof_10141618_bonitasprings_blue_Bonita-Springs-Blue-Sofa.jpeg', NULL, NULL, NULL, NULL, '2018-05-07 04:40:07', '2018-05-07 04:40:07'),
(42, 4, 'jeep', 'Others', '500000', '2018-05-08 18:00:00', 'Prior to 1940 the term \"jeep\" had been used as U.S. Army slang for new recruits or vehicles', 'product_picture/2018-Jeep-Wrangler-Unlimited-Sahara.jpg', NULL, NULL, NULL, NULL, '2018-05-07 06:47:47', '2018-05-07 06:47:47'),
(43, 4, 'jeep', 'Others', '500000', '2018-05-10 18:00:00', 'Prior to 1940 the term \"jeep\" had been used as U.S. Army slang for new recruits or vehicles', 'product_picture/download (2).jpg', NULL, NULL, NULL, NULL, '2018-05-07 06:48:11', '2018-05-07 06:48:11'),
(44, 4, 'jeep wrangler', 'Others', '10000000', '2018-05-07 18:00:00', 'Prior to 1940 the term \"jeep\" had been used as U.S. Army slang for new recruits or vehicles', 'product_picture/hyundai-creta.jpg', NULL, NULL, NULL, NULL, '2018-05-07 06:48:35', '2018-05-07 06:48:35'),
(45, 4, 'Jeep Compass', 'Others', '4000000', '2018-05-11 18:00:00', 'Prior to 1940 the term \"jeep\" had been used as U.S. Army slang for new recruits or vehicles', 'product_picture/jeep-compass.jpg', NULL, NULL, NULL, NULL, '2018-05-07 06:49:20', '2018-05-07 06:49:20'),
(46, 4, 'wallmate', 'Others', '5000', '2018-05-08 18:00:00', 'grotesque or bizarre.', 'product_picture/Antic-Ganesh-Wallpaper.jpg', NULL, NULL, NULL, NULL, '2018-05-07 06:50:52', '2018-05-07 06:50:52'),
(47, 4, 'used chair', 'Others', '1500', '2018-05-09 18:00:00', 'grotesque or bizarre.', 'product_picture/me9116603-antic-wood-chair-desert-india-hd-a0106.jpg', NULL, NULL, NULL, NULL, '2018-05-07 06:51:28', '2018-05-07 06:51:28'),
(48, 4, 'platinum ring', 'Others', '50000', '2018-05-10 18:00:00', 'grotesque or bizarre and intact.', 'product_picture/images (4).jpg', NULL, NULL, NULL, NULL, '2018-05-07 06:52:06', '2018-05-07 06:52:06'),
(53, 4, 'Headphone', 'Hardware', '2000', '2018-05-01 18:00:00', 'Headphones (or head-phones in the early days of telephony and radio) are a pair of small loudspeaker', 'product_picture/download.jpg', NULL, NULL, NULL, NULL, '2018-05-07 07:06:32', '2018-05-07 07:06:32'),
(55, 4, 'Headphone new', 'Hardware', '2000', '2018-04-29 18:00:00', 'Headphones (or head-phones in the early days of telephony and radio) are a pair of small loudspeaker', 'product_picture/images (1).jpg', NULL, NULL, NULL, NULL, '2018-05-07 07:07:13', '2018-05-07 07:07:13'),
(56, 4, 'new headphone', 'Hardware', '2000', '2018-05-08 18:00:00', 'Headphones (or head-phones in the early days of telephony and radio) are a pair of small loudspeaker', 'product_picture/images (2).jpg', NULL, NULL, NULL, NULL, '2018-05-07 07:07:47', '2018-05-07 07:07:47');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `name`, `category`, `price`, `end_time`, `description`, `image1`, `image2`, `image3`, `image4`, `image5`, `created_at`, `updated_at`) VALUES
(22, 4, 'fridge', 'Household accessories', '12000', '2018-05-09 18:00:00', 'original', 'product_picture/LG_Electronics-2698756674-GF-B620PL_01_Large.jpg', NULL, NULL, NULL, NULL, '2018-05-07 00:41:43', '2018-05-07 00:41:43'),
(23, 4, 'sofa', 'Household accessories', '2000', '2018-05-10 18:00:00', 'portable telephone that can make and receive calls over a radio frequency link', 'product_picture/download (2).jpg', NULL, NULL, NULL, NULL, '2018-05-07 01:18:45', '2018-05-07 01:18:45'),
(24, 4, 'mobile', 'Mobile', '10000', '2018-05-07 18:00:00', 'portable telephone that can make and receive calls over a radio frequency link', 'product_picture/samsung-mobile-500x500.jpg', NULL, NULL, NULL, NULL, '2018-05-07 01:18:47', '2018-05-07 01:18:47'),
(25, 4, 'mobile', 'Mobile', '1500', '2018-05-14 18:00:00', 'portable telephone that can make and receive calls over a radio frequency link', 'product_picture/images (1).jpg', NULL, NULL, NULL, NULL, '2018-05-07 01:18:50', '2018-05-07 01:18:50'),
(26, 4, 'Camera DSLR', 'Electronics', '15000', '2018-05-09 18:00:00', 'portable telephone that can make and receive calls over a radio frequency link', 'product_picture/download (6).jpg', NULL, NULL, NULL, NULL, '2018-05-07 01:23:43', '2018-05-07 01:23:43'),
(27, 4, 'Camera DSLR', 'Electronics', '50000', '2018-05-11 18:00:00', 'portable telephone that can make and receive calls over a radio frequency link', 'product_picture/download (4).jpg', NULL, NULL, NULL, NULL, '2018-05-07 01:23:46', '2018-05-07 01:23:46'),
(28, 4, 'baby toy', 'Babies', '500', '2018-05-10 18:00:00', 'Once your baby starts moving, you need toys that can keep up with them!', 'product_picture/images (4).jpg', NULL, NULL, NULL, NULL, '2018-05-07 02:35:10', '2018-05-07 02:35:10'),
(29, 4, 'baby toy', 'Babies', '40000', '2018-05-07 18:00:00', 'while the user is moving within a telephone service area. The radio frequency link establishes', 'product_picture/download (7).jpg', NULL, NULL, NULL, NULL, '2018-05-07 02:35:13', '2018-05-07 02:35:13'),
(30, 4, 'camera nikon', 'Electronics', '2000', '2018-05-22 18:00:00', 'portable telephone that can make and receive calls over a radio frequency link', 'product_picture/5580130_sd.jpg', NULL, NULL, NULL, NULL, '2018-05-07 02:35:16', '2018-05-07 02:35:16'),
(31, 4, 'compressor of fridges', 'Household accessories', '3000', '2018-05-08 18:00:00', 'Refrigerator problem? It could be the compressor (expensive!) but before you call the repair service, try these simple repairs.', 'product_picture/21320.jpg', NULL, NULL, NULL, NULL, '2018-05-07 03:34:31', '2018-05-07 03:34:31'),
(32, 4, 'Refigerator', 'Electronics', '25000', '2018-05-08 18:00:00', 'Refrigerator problem? It could be the compressor (expensive!) but before you call the repair service, try these simple repairs', 'product_picture/22cfa081acf45e93f628fc34bc97b064.jpg', NULL, NULL, NULL, NULL, '2018-05-07 03:35:40', '2018-05-07 03:35:40'),
(33, 4, 'samsung mobile', 'Mobile', '10000', '2018-05-09 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/global-mobile-phone-sales-forecasted-by-88-countries.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:08:57', '2018-05-07 04:08:57'),
(34, 4, 'Mobile', 'Mobile', '10000', '2018-05-08 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/apple-iphone-5s_1378837974__450_400.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:09:00', '2018-05-07 04:09:00'),
(35, 4, 'Grinder', 'Household accessories', '3000', '2018-05-09 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/Electronics-Home-Appliance-Bangalore-Usha-International-1bPXxn7A-4f339ac022a35_regular.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:09:03', '2018-05-07 04:09:03'),
(36, 4, 'Grinder', 'Household accessories', '3000', '2018-05-08 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/images (5).jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:09:06', '2018-05-07 04:09:06'),
(37, 4, 'wet grinder', 'Household accessories', '1500', '2018-05-08 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/1034346_Other-670x481_GPS._V533474862_.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:09:08', '2018-05-07 04:09:08'),
(38, 4, 'homor decor', 'Household accessories', '2000', '2018-05-08 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/y-green-home-decor-design-green-home-accessories-11.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:09:11', '2018-05-07 04:09:11'),
(39, 4, 'desktop', 'Computer', '40000', '2018-05-10 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/images (1).jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:15:28', '2018-05-07 04:15:28'),
(40, 4, 'desktop', 'Computer', '30000', '2018-05-08 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/Awesome-Setting-Up-Shop-Part-4-A-Writers-Computer-The-Writing-Nut.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:15:31', '2018-05-07 04:15:31'),
(41, 4, 'mac book', 'Computer', '60000', '2018-05-10 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/mac_doctors-800x350.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:15:35', '2018-05-07 04:15:35'),
(42, 4, 'mac', 'Computer', '50000', '2018-05-08 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/desktop_mac_full_view_alt.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:15:37', '2018-05-07 04:15:37'),
(43, 4, 'mobile', 'Mobile', '10000', '2018-05-08 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/s6_55e7cec344023._samsung-galaxy-s6-edge-32-gb-mobile-phone.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:41:26', '2018-05-07 04:41:26'),
(44, 4, 'toy', 'Babies', '1500', '2018-04-30 18:00:00', 'Cleaning supplies. Rags. Paper towels. Windex. Clorox wipes. Stove cleaner. Rubber Gloves.', 'product_picture/download.jpg', NULL, NULL, NULL, NULL, '2018-05-07 04:42:52', '2018-05-07 04:42:52'),
(45, 4, 'Electronics set', 'Electronics', '10000', '2018-05-09 18:00:00', 'New Electronics brings you the latest product and component news from market leading suppliers.', 'product_picture/BP_2014_51_1.jpg', NULL, NULL, NULL, NULL, '2018-05-07 06:56:30', '2018-05-07 06:56:30'),
(46, 4, 'Electronics set', 'Electronics', '10000', '2018-05-08 18:00:00', 'New Electronics brings you the latest product and component news from market leading suppliers.', 'product_picture/1449392214.jpg', NULL, NULL, NULL, NULL, '2018-05-07 06:56:33', '2018-05-07 06:56:33'),
(47, 4, 'smartphone adapter', 'Electronics', '2000', '2018-05-08 18:00:00', 'New Electronics brings you the latest product and component news from market leading suppliers.', 'product_picture/23623009c096a4a6204699d68611e6ae.jpg', NULL, NULL, NULL, NULL, '2018-05-07 06:56:35', '2018-05-07 06:56:35'),
(48, 4, 'Headphone', 'Hardware', '1500', '2018-04-30 18:00:00', 'Headphones (or head-phones in the early days of telephony and radio) are a pair of small loudspeaker', 'product_picture/41m-yVNdofL._SL500_AC_SS350_.jpg', NULL, NULL, NULL, NULL, '2018-05-07 07:09:42', '2018-05-07 07:09:42'),
(49, 4, 'Headphone', 'Hardware', '2000', '2018-04-30 18:00:00', 'Headphones (or head-phones in the early days of telephony and radio) are a pair of small loudspeaker', 'product_picture/HEARS with headset.jpg', NULL, NULL, NULL, NULL, '2018-05-07 07:09:47', '2018-05-07 07:09:47'),
(50, 4, 'New headphone', 'Hardware', '2000', '2018-05-10 18:00:00', 'Headphones (or head-phones in the early days of telephony and radio) are a pair of small loudspeaker', 'product_picture/ovleng-headphone-s99-500x500.jpg', NULL, NULL, NULL, NULL, '2018-05-07 07:09:51', '2018-05-07 07:09:51'),
(51, 4, 'New headphone', 'Hardware', '2000', '2018-05-10 18:00:00', 'Headphones (or head-phones in the early days of telephony and radio) are a pair of small loudspeaker', 'product_picture/images.jpg', NULL, NULL, NULL, NULL, '2018-05-07 07:09:53', '2018-05-07 07:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headline` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user_type`, `email`, `phone`, `address`, `headline`, `image`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'a', 'buyer', 'a@gmail.com', '01777490966', '33/12, Mirpur-12, Dhaka', NULL, 'avatars/images.jpeg', '$2y$10$aHRi0sbP2ORwdiWQ8ivPa.ysyfeEQaCJkKtPAptTNB8zZoUUQI1ci', 'CFXvKHJtALWt8NoN6v1Weu4tm12JlouUZ354qe00Swr1pvqxGGkZbAIqMFgf', '2018-04-24 11:57:25', '2018-04-24 11:57:25'),
(4, 's', 'seller', 's@gmail.com', '017774909655', '33/12, Mirpur-12, Dhaka', NULL, 'avatars/img.jpeg', '$2y$10$qUNZ9ZVTIyT6g4HTNsa3.eUpiN18.hUvsNywhmBPNKr/rNwYUL8fq', 'E4scaYDhfpvhocL7ZRI6BOvStWoGanPOCNdOl408ARYA4ZHAtK8SwNosSiGZ', '2018-04-24 12:22:44', '2018-04-24 12:22:44'),
(9, 'ram', 'seller', 'sam@gmail.com', '4545454', 'dhaka', NULL, 'avatars/pexels-photo-979722.jpeg', '1234567', 'Ck7FYntK6erNQHkSZa2tRwF0JmPc9SorpSOhMhMk62anDz5DAJ5Xk4mcWwe3', '2018-04-24 12:27:48', '2018-04-24 12:27:48'),
(10, 'hjhjh', 'buyer', 'd@gmail.com', '56657', 'nmnmn', NULL, 'avatars/desktop-abstract-wallpapers-in-hd-download.jpg', '123456', '65nRFNWvzGngtybLL2S05uyUqzw9ul92qcjltVfCmyApbFYYzEtH8CVxFmg2', '2018-04-24 12:33:05', '2018-04-24 12:33:05'),
(11, 'vat', 'buyer', 'vat69@gmail.com', '01777454566', '12/8 dhaka-1207', NULL, 'avatars/images (4).jpg', '123456', 'jHWquq3cso55JI1oc8s90NYqnRIEgQaCUoDYM6pnnviBiAEJd1uB6RqeLm2z', '2018-04-24 13:21:11', '2018-04-24 13:21:11'),
(12, 'micky', 'buyer', 'micky@gmail.com', '1236547', 'dhaka 1209-90/d', NULL, 'avatars/download (1).png', '123456', 'eC6DlbZhcEbsVS2M1VmEJJ2x76eEbOpkXIghGFKXm6xTWYpntDbxL0cRrcgD', '2018-04-25 09:05:23', '2018-04-25 09:05:23'),
(13, 'ami', 'seller', 'ami@gmail.com', '5454541', 'dhaka 1209-90/d', NULL, 'avatars/pexels-photo-220453.jpeg', '123456', 'AF82aa97uD0WXH5cQtgOkQ9HvCrDuo1cknnLYoOiZNAFWolEhYkMUFpw1jaN', '2018-05-06 22:11:27', '2018-05-06 22:11:27'),
(14, 'ramm', 'buyer', 'raami@gmail.com', '65655654', '12/8 dhaka-1207', NULL, 'avatars/download.jpg', '123456', '11LeYrT3jxtE0wyYzN79YdffOrdZtySLJHf1p7KNXHCFiQegzUxG2R8T7dX5', '2018-05-06 22:15:58', '2018-05-06 22:15:59'),
(15, 'moor', 'buyer', 'moonphysics11@gmail.com', '6565622', 'dhaka-mirpur 1207', NULL, 'avatars/images.jpg', '123456', 'Jn7B6XMnLG26ffNhWMtAhMis8UtORtYK0XDFHGIObKc1OWsDzSyE59mRa8b2', '2018-05-06 22:20:43', '2018-05-06 22:20:43'),
(16, 'dada', 'buyer', 'dada@gmail.com', '54545', '12/8 dhaka-1207', NULL, 'avatars/images.jpg', '$2y$10$A6Xie3F/RnJGMenY6J//OOuuplACA3bkYSSFNnoJYTjhZlFmTXfj6', 'J3UlHDA4tA7KPXUN3I2lUQ4SNvb1E3eFAIbHK0V67SEavtoG4Svb5I2klZvO', '2018-05-06 23:54:13', '2018-05-06 23:54:13'),
(17, 'mahem', 'buyer', 'ratxerox@gmail.com', '01754554545', '12/8 dhaka-1207', NULL, 'avatars/141121150043-morgan-freeman-2014-exlarge-169.jpg', '$2y$10$Dl8Rh0qIhmdftusdtzlMmOY1Z7p5GBBmV5fqy/0O5piODGpLU7yJC', '7zvKTkgEQlVSfy7gSbdLzuf9958uzZkQrQEauctg93Jc6JdTZEEwWc4gdIRq', '2018-05-07 00:44:46', '2018-05-07 00:44:46'),
(18, 'moor', 'buyer', 'moor@gmail.com', '122545', 'dhaka-1207', NULL, 'avatars/download.jpg', '$2y$10$bpZW/gOZqFYzQFe3CPA8JeVU2KEbEHohlHhZ6PSmtDTvBxeggCCNe', 'd5NkW0yjZCiaAAspQ6A6fY2oaRr6Tp0qN2KyKigSFzlBw2Qi7rCNDq7y6G8P', '2018-05-07 01:30:13', '2018-05-07 01:30:13'),
(19, 'mehdi', 'buyer', 'mehedi@gmail.com', '014545502', '12/8 dhaka-1207', NULL, 'avatars/download (1).jpg', '$2y$10$l.uDhoMMAPo7W.7Pz77aiuV1zzQvbGTF75YsXkPCRW1MKAnaWSrGu', 'u8B8mvzu0Dp1fI44bfbBfGNMOXQJjKtPsmN4EHcttxwPBMTfdCrVbr1hyDBI', '2018-05-07 07:12:25', '2018-05-07 07:12:25'),
(20, 'paady', 'buyer', 'paddy@gmail.com', '54554562', 'dhaka-1207', NULL, 'avatars/rhinehart_2552984b.jpg', '$2y$10$ZvjYbpjMdHkh8kFXW8ekyuWyY6t6rznUIpXHUnI32kw8rkJ7YOwW6', 'vWTx3SVxHTjGswIuoZZyvCy3XkSNUJSlsNhrck4fLzPu88nZ7RQubJL520l6', '2018-05-07 07:25:02', '2018-05-07 07:25:02'),
(21, 'meddy', 'buyer', 'meddy@gmail.com', '564521', 'dhaka-mirpur 1207', NULL, 'avatars/images (3).jpg', '$2y$10$PhbKSXAyI/i.m46c1rKrEeuWb8zEoJCyJUOm8PqpWwRFDNdZRtE8S', 'a6EbpQfvrTCEFRKmL2H2Tyw28pRHfcnLcRZLnpWQwlWypmC6g6kRfZaSlBjq', '2018-05-07 07:29:09', '2018-05-07 07:29:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_username_unique` (`username`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `bids`
--
ALTER TABLE `bids`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bids_user_id_foreign` (`user_id`),
  ADD KEY `bids_product_id_foreign` (`product_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pending_products`
--
ALTER TABLE `pending_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pending_products_user_id_foreign` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bids`
--
ALTER TABLE `bids`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pending_products`
--
ALTER TABLE `pending_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bids`
--
ALTER TABLE `bids`
  ADD CONSTRAINT `bids_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bids_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pending_products`
--
ALTER TABLE `pending_products`
  ADD CONSTRAINT `pending_products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
