<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/signin', array('as' => 'user.signin', 'uses' => 'SignInController@doLogin'));

Route::get('/about',function() {
   return view('about');
});

Auth::routes();

Route::get('/buyer/product_desc/{id}', function ($id) {
    $product = \App\Product::find($id);
    return view('buyer.product_desc')->with('product', $product);
});

Route::group(['middleware' => ['auth']], function () {
    
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/buyers_home', 'BuyersHome@index')->name('buyers_home');
    Route::get('/products', 'BuyersHome@products')->name('products');
    Route::get('/mybids', 'BuyersHome@mybids')->name('mybids');

    Route::post('/placebid/{id}', 'BuyersHome@placebid')->name('placebid');

    Route::get('/see_bidders/{id}', 'SellerController@see_bidders');

    Route::post('/award/{bid_id}', 'SellerController@award');

    Route::get('/sellers_home', 'SellerController@index')->name('sellers_home');
    Route::get('/', 'SellerController@createSale')->name('create_product');
    Route::get('/create_product', 'SellerController@createSale')->name('create_product');
    Route::post('/post_product', 'SellerController@storeSale')->name('post_product');
    Route::get('/myproducts', 'SellerController@myproducts')->name('myproducts');
    Route::get('/my_pending_products', 'SellerController@myPendingProducts')->name('my_pending_products');
    Route::get('/seller/product_desc/{id}', 'SellerController@product_desc');
    Route::get('/seller/pending_product_desc/{id}', 'SellerController@pending_product_desc');

    Route::get('/edit_product/{id}', 'SellerController@edit_product');
    Route::post('/update_product/{id}', 'SellerController@update_product')->name('update_product');
    Route::get('/delete_product/{id}', 'SellerController@delete_product');

    Route::get('/delete_pending_product/{id}', 'SellerController@delete_pending_product');

    Route::get('seller/profile','SellerController@profile')->name('sellers_profile');
    Route::get('buyer/profile','BuyersHome@profile')->name('buyers_profile');

});




Route::group(['prefix' => 'webadmin'], function () {

    Route::post('/register', 'AdminAuth\RegisterController@create');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::get('/login', 'AdminAuth\LoginController@index')->name('webadmin_login');// other routes...

    Route::group(['middleware' => 'auth.admin'], function () {
        Route::post('logout', 'AdminAuth\LoginController@logout')->name('webadmin_logout');

        Route::get('home', 'AdminController@index')->name('webadmin_home');
        Route::get('/', 'AdminController@index')->name('webadmin_home');
        Route::get('/profile','AdminController@profile');

        Route::get('/products', 'AdminController@products')->name('webadmin_products');
        Route::get('/pending_products', 'AdminController@pending_products');
        Route::get('/product_desc/{id}', 'AdminController@product_desc');
        Route::get('/pending_product_desc/{id}', 'AdminController@pending_product_desc');

        Route::get('approve_pending_product/{id}', 'AdminController@approve_pending_product');
        Route::get('delete_pending_product/{id}', 'AdminController@delete_pending_product');

        Route::get('/see_bidders/{id}', 'AdminController@see_bidders');

        Route::post('/award/{bid_id}', 'AdminController@award');

        Route::get('/categories', 'CategoryController@index');

        Route::get('/add_category', 'CategoryController@create');
        Route::post('/category', 'CategoryController@store');
        Route::get('/delete_category/{id}', 'CategoryController@delete');

        Route::get('/edit_product/{id}', 'AdminController@edit_product');
        Route::post('/update_product/{id}', 'AdminController@update_product');
        Route::get('/delete_product/{id}', 'AdminController@delete_product');

    });
});